Tests
=====

.. warning::
   On 👏 écrit 👏 tout 👏 le 👏 temps 👏 des 👏 tests.

Ils sont éxecutés par pytest_ avec une petite ribambelle de greffons pour (normalement)
nous simplifier la vie: 

* pytest-django_
* pytest-factoryboy_
* pytest-pythonpath_

Merci de lire les documentations pour en tirer le meilleur parti.

En complement des tests on en profite pour valider le style & le typage avec : 

* pytest-mypy_
* pytest-flake8_

Enfin, pytest-cov_ génère le compte rendu de couverture des tests (le `rapport html`_ est
dans le dossier ``htmlcov``), si le taux déscend en dessous de 90% (cf. setup.cfg) les 
tests échoueront.

.. _pytest: https://docs.pytest.org/en/latest/
.. _pytest-django: pytest-django.readthedocs.io/
.. _pytest-factoryboy: https://github.com/pytest-dev/pytest-factoryboy
.. _pytest-pythonpath: https://github.com/bigsassy/pytest-pythonpath
.. _pytest-mypy: https://github.com/dbader/pytest-mypy
.. _pytest-flake8: https://github.com/tholo/pytest-flake8
.. _pytest-cov: https://github.com/pytest-dev/pytest-cov
.. _rapport html: ./htmlcov/index.html

Pour lancer les tests sur l'environnement de développement :

.. code-block::

    # lancer pytest avec les paramètres par défaut
    make unit_tests
    # passer des paramètres supplémentaires à pytest
    make unit_tests args="--lf --pdb"
