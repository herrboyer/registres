Backend Registres
=================

Cette documentation est à l'attention des contributeurs au projet, elle
a pour but d'en faciliter la prise en main.

Le backend Registres est là pour faire l'interface entre le frontend_
et les données.

C'est un projet monté autour de django_ et de plusieurs applications /
paquets, notamment `Django REST framework`_ pour mettre en place une API.

.. _django : https://www.djangoproject.com/
.. _frontend : https://gitlab.com/registres/frontend
.. _Django REST framework : https://www.django-rest-framework.org

Le tout est packagé avec docker puis testé et déployé avec GitLab.

.. toctree::
   :maxdepth: 2
   :caption: Table des matières:

   dev
   api
   tests
   settings
   applications/core
   applications/registers


Index
=====

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
