"""Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""

import os
import sys

import django

sys.path.insert(0, os.path.abspath("../"))
os.environ["DJANGO_SETTINGS_MODULE"] = "app.settings"
django.setup()

project = "Backend Registres"
copyright = "2022, Anybox"
author = "Anybox"

release = django.conf.settings.VERSION

extensions = ["sphinx.ext.autodoc", "sphinxcontrib_django"]

templates_path = ["_templates"]

language = "fr"

exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

html_theme = "sphinx_rtd_theme"

# html_theme_options = {}
