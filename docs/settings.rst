Paramétrage
===========

Le module dédié au paramétrage du projet registres.

Paramètres
----------

.. literalinclude:: ../app/settings.py
   :language: python

Routage
----------

HTTP
````

.. literalinclude:: ../app/urls.py
   :language: python

