API
===

L'API de registres est proposée au format REST sur ``/api``.

La documentation complète (et interactive) est accessible sur http://localhost:8000/api

Pour s'authentifier sur swagger il faut récupérer un jeton "access" jwt et le coller
dans le formulaire "Authorize" **préfixé** de ``Bearer``. Exemple :

.. code-block::

   # avec httpie
   http post http://localhost:8000/api/token/ email=dpo@anybox.fr password=taupe_secret
   # réponse : {
   # "access": "eyJ0ebd...v0YM", 
   # "refresh": "eyJ0eZ...Rw4co"
   # }

La valeur de l'authentification sera ``Bearer eyJ0ebd...v0YM``

Organisation générale de l'api
------------------------------

La documentation donne le détail, mais dans les grandes lignes, l'api est divisée en
plusieurs parties :

* ``users`` pour tout ce qui concerne les comptes utilisateur
* ``organisations`` pour les organisation et tout ce qui y est lié (membres, mesures de
  sécurité, catégories de données, etc.)
* ``registers`` pour tout ce qui concerne les registres

L'api est sur un modèle d'emboîtement, par exemple pour organisation on a :

* ``api/organisations/``
* ``api/organisations/<organisation_pk>/members``
* ``api/organisations/<organisation_pk>/members/<member_pk>``
* ``api/organisations/<organisation_pk>/recipients``
* ``api/organisations/<organisation_pk>/recipients/<recipient_pk>``
* etc.
* ``api/organisations/<organisation_pk>/registers/...``

On s'arrête au niveau ``list`` des registers, avec assez peu de données sur les
différents registres (controller, etc.).

Pour ne pas avoir d'url à rallonge on retourne à la racine pour le détail des registres :

* ``api/registers/controllers/<register_pk>``
* ``api/registers/controllers/<register_pk>/processings``
* ``api/registers/controllers/<register_pk>/processings/<processing_pk>``
* etc.

Le cas du chameau
-----------------

.. warning::
   L'API est servie *camelCasée* mais on y accède en snake_case depuis le backend 

Le snake_case utilisé pour le nommage des champs en python est automatiquement transformé
en camelCase par djangorestframework-camel-case_ ce qui permet de respecter pep8 côté
backend et les convention REST et JS pour le front (spéciale dédicace à TypeScript).

.. _djangorestframework-camel-case : https://github.com/vbabiy/djangorestframework-camel-case
