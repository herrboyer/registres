Registres
=========

Tout ce qui concerne la gestion des registres.

Modèles
--------

.. automodule:: registers.models.base
    :members:

Registre des violations
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.models.breaches
    :members:

Registre de traitement
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.models.controllers
    :members:

Registre de sous-traitance
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.models.processors
    :members:

Registre des demandes
~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.models.requests
    :members:

Vues
----

.. automodule:: registers.views.base
    :members:

Registre des violations
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.views.breaches
    :members:

Registre de traitement
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.views.controllers
    :members:

Registre de sous-traitance
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.views.processors
    :members:

Registre des demandes
~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.views.requests
    :members:

Convertisseurs
--------------

.. automodule:: registers.serializers.base
    :members:

Registre des violations
~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.serializers.breaches
    :members:

Registre de traitement
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.serializers.controllers
    :members:

Registre de sous-traitance
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.serializers.processors
    :members:

Registre des demandes
~~~~~~~~~~~~~~~~~~~~~

.. automodule:: registers.serializers.requests
    :members: