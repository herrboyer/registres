Core
====

Le cœur du backend, contient tout ce qui est transverse aux autres
applications.

Modèles
-------

.. automodule:: core.models
    :members:

Outils
------

.. automodule:: core.helpers
    :members:

Vues
----

.. automodule:: core.views
    :members:

Convertisseurs
--------------

.. automodule:: core.serializers
    :members: