Développement
=============

Le backend de registres est une application django dockerisée.

Contribuer au projet
--------------------

Merci de prendre connaissance du `code de conduite`_.

.. _code de conduite : https://gitlab.com/registres/app/-/blob/master/CODE_OF_CONDUCT.md

Conventions
-----------

Le code python est conforme aux recommandation `PEP 8`_ et typé:

* formatage par black_
* validation par flake8_
* typage par mypy_


.. _PEP 8 : https://www.python.org/dev/peps/pep-0008/
.. _black : https://github.com/psf/black
.. _flake8 : https://flake8.pycqa.org/en/latest/
.. _mypy: http://mypy-lang.org/

Dépendances
-----------

.. warning::
   Ne pas modifier directement les requirements*.txt

Les dépendances sont listées dans les fichiers ``project/*.in`` et compilées par
`pip-tools`_. Chaque fichier correspond à un contexte différent :

* ``project/requirements.in`` -> ``requirements.txt`` :
  les dépendances générales
* ``project/requirements.dev.in`` -> ``requirements.dev.txt`` :
  les dépendances de développement

Les failles de sécurité sur les dépendances sont vérifiées de manière
hébdomadaire par `safety`_ sur la CI.

.. _pip-tools : https://github.com/jazzband/pip-tools
.. _safety : https://pypi.org/project/safety/

Pour mettre à jour les dépendances :

.. code-block::

    # équivalent d'un ``pip-compile --upgrade`` sur tous les fichiers ``.in``
    make requirements

Versionnage
-----------

On utilise `la gestion sémantique de version`_ pour numéroter les versions de l'app.
Il est important d'incrémenter le numéro de version dans chaque merge request, un test
unitaire ira verifier que le numéro choisi est bien supérieur à celui actuellement sur
la branche ``master`` du dépôt.

.. _la gestion sémantique de version : https://semver.org/lang/fr/

Pour incrémenter automatiser le numéro de version (dans settings.py et setup.cfg) et le
commit-er grâce à bump2version_.

.. _bump2version : https://github.com/c4urself/bump2version

.. code-block::

    # passe x.y.z en x.y.z+1
    make bumpversion args="patch"
    # passe x.y.z en x.y+1.0
    make bumpversion args="minor"
    # passe x.y.z en x+1.0.0
    make bumpversion args="major"
    # simuler les changements
    make bumpversion args="--dry-run --verbose"

Un tag Git est automatiquement créé par la CI au moment du l'empaquetage de
l'application, cf. job `release_package`.

CI/CD
-----

L'intégration et le développement continus sont en place sur GitLab_.
Le fichier ``project/gitlab-ci.yml`` décrit l'ensemble des stages et tâches
exécutées :

* testing : les tests unitaires + lint
* building : construction d'une image docker
* deploying : notification du dépôt ``app`` d'une mise à jour et déploiement de
  la documentation

.. _GitLab : https://gitlab.com/registres/backend

Docker
------

Plusieurs services en place sur la stack (cf. ``docker-compose.yml``):

* ``db`` : la base PostgreSQL
* ``backend`` : le backend django
* ``migrate`` : un service « one-shot » qui applique les migrations sur la base de
  données si besoin

Les dossiers locaux sont mappés avec les volumes docker : les modifications
faites en local sont appliquées immédiatement sur les services.

Environnement de développement
------------------------------

Le paramétrage de l'instance se fait au travers de variables d'environnement
chargées par les ``settings`` django. Le plus simple est de dupliquer le
fichier ``project/env.tpl`` en ``.env`` et de le modifier pour correspondre
aux besoins. Une description détaillée des variables est faite dans
``settings.py``.

Pour lancer l'environnement de développement (et après avoir installé docker et
docker-compose) :

.. code-block::

    # équivalent docker-compose up
    make run
    # quitter avec ctrl+c

.. note::
   Plusieurs commandes utiles sont disponibles dans le ``Makefile`` utilisez
   ``make help`` pour en savoir plus.

Le backend est accessible à l'adresse http://localhost:8000 et la console affiche les
logs des différents services.

Fusions
-------

Le dépôt est géré par Git sur GitLab. On utilise le mécanisme de ``Merge Request`` pour
développer de nouvelles fonctionnalités (et corriger les bugs).

* impossible de pousser directement sur master
* il est **chaudement** recommandé de faire relire les merge requests par un tiers avant
  de les fusionner
* les commits sont à squasher avant le merge
