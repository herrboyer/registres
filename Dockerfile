# Image de base
FROM python:3.11-slim as base
LABEL maintainer="rboyer@anybox.fr"

## Variables d'env
ENV DJANGO_SETTINGS_MODULE="app.settings" \
    PYTHONPYCACHEPREFIX="/tmp/python_cache" \
    VIRTUAL_ENV="/opt/registres/venv" \
    PATH="${VIRTUAL_ENV}:${PATH}" \
    TAILWIND_CLI_URL="https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-linux-x64"

## Copie des fichiers, note: c'est le .dockerignore qui filtre ce que l'on va copier
WORKDIR /opt/registres/app
COPY . .

## Dépendances de prod
RUN python -m venv $VIRTUAL_ENV && \
    pip install --no-cache-dir -r requirements.txt && \
    chmod +x migrate.sh
EXPOSE 8000

# Image de test
FROM base AS test

## Ajout des dépendances de dev
RUN pip install --no-cache-dir -r requirements.dev.txt

# Image de dev
FROM test AS dev

## Récupération et installation de la CLI tailwind
RUN python3 -c "from urllib.request import urlretrieve; urlretrieve('${TAILWIND_CLI_URL}', 'tailwindcss-linux-x64')" && \
    chmod +x tailwindcss-linux-x64 && \
    mv tailwindcss-linux-x64 /usr/local/bin/tailwindcss