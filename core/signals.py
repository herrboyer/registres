"""Signaux."""

from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_migrate
from django.dispatch import receiver
from django.http import HttpRequest
from django.contrib.auth import get_user_model
from django.conf import settings


@receiver(user_logged_in)
def populate_session(
    sender: get_user_model, request: HttpRequest, user: get_user_model, **kwargs
) -> None:
    """Après authentification on charge dans la session les infos de l'utilisateur.

    - organisation_id
    - controller_register_id
    """
    if default_organisation := user.organisation_set.first():
        request.session["organisation_id"] = str(default_organisation.pk)
        request.session["controller_register_id"] = str(
            default_organisation.controller_register.pk
        )


@receiver(post_migrate)
def initial_data(sender, **kwargs) -> None:
    """Création des données initiales.

    - Organisation par défaut & utilisateur
    """
    from core.models import Organisation, User

    default_user = settings.DEFAULT_USER
    try:
        user = User.objects.get(email=default_user["email"])
    except User.DoesNotExist:
        user = User.objects.create_superuser(**default_user)
    default_organisation = settings.DEFAULT_ORGANISATION
    try:
        Organisation.objects.get(name=default_organisation["name"])
    except Organisation.DoesNotExist:
        Organisation.objects.create_with_fixtures(**default_organisation, user=user)
