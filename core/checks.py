"""Vérifications système django.

Modèles :

* anybox.E001: Le champ n'a pas de nom verbeux.
* anybox.E002: Les noms verbeux doivent utiliser gettext.
* anybox.E003: Les aides contextuelles doivent utiliser gettext.
* anybox.E004: Le modèle n’a pas de nom verbeux.
* anybox.E005: Le nom verbeux du modèle n’utilise pas gettext.
* anybox.E006: Le modèle n’a pas de nom verbeux pluriel.
* anybox.E007: Le nom verbeux pluriel du modèle n’utilise pas gettext.
* anybox.W001: Le modèle n’a pas de Meta.

**Très** largement inspiré de :
https://hakibenita.com/automating-the-boring-stuff-in-django-using-the-check-framework
"""

import ast
import inspect
from textwrap import dedent
from typing import Iterable, Optional

import django.apps
import django.core.checks
from django.core.exceptions import FieldDoesNotExist


def get_kwarg(node: ast.Assign, arg: str) -> Optional[ast.Assign]:
    """Récupération d'un argument par clé sur un nœud."""
    for kw in node.value.keywords:  # type: ignore
        if kw.arg == arg:
            return kw
    return None


def get_arg(node: ast.Assign, number: int = 0) -> Optional[ast.Assign]:
    """Récupération d'un argument par position sur un nœud."""
    try:
        return node.value.args[number]  # type: ignore
    except IndexError:
        return None


def is_gettext_node(node: ast.Assign) -> bool:
    """True si la fonction du nœud est _."""
    if hasattr(node, "value"):  # ast.Name ou ast.Keyword
        node = node.value  # type: ignore

    if not isinstance(node, ast.Call):
        return False

    if not node.func.id == "_":  # type: ignore
        return False
    return True


def check_verbose_name(field, node):
    """Vérification des libellés / aides contextuelles."""
    if field.name == "id":  # pas d'importance pour les ID
        return

    if field.is_relation:  # pas d'importance pour les fk / m2m
        return

    # Pour définir un verbose_name soit on utilise un argument nommé verbose_name
    # soit on le passe premier argument du champ.
    verbose_name = get_kwarg(node, "verbose_name") or get_arg(node, number=0)
    if verbose_name is None:
        yield django.core.checks.Error(
            "Le champ n'a pas de nom verbeux.",
            hint="Ajouter verbose_name au champ.",
            obj=field,
            id="anybox.E001",
        )
    else:
        if not is_gettext_node(verbose_name):
            yield django.core.checks.Error(
                "Les noms verbeux doivent utiliser gettext.",
                hint="Ajouter _() autour du verbose_name actuel.",
                obj=field,
                id="anybox.E002",
            )
    help_text = get_kwarg(node, "help_text")
    if help_text is not None:
        if not is_gettext_node(help_text):
            yield django.core.checks.Error(
                "Les aides contextuelles doivent utiliser gettext.",
                hint="Ajouter _() autour du help_text actuel.",
                obj=field,
                id="anybox.E003",
            )


def check_model(model) -> Iterable[django.core.checks.CheckMessage]:
    """Vérification d'un modèle.

    Exécute les règles de vérification et retourne les messages d'erreur.
    """
    model_source = inspect.getsource(model)
    # dedent sert particulièrement pendant les tests car les modèles sont dans des
    # méthodes et ast.parse se plaignait de IndentationError: unexpected indent
    model_node = ast.parse(dedent(model_source))
    assert isinstance(model_node, ast.Module)

    class_meta = None
    for node in model_node.body[0].body:  # type: ignore
        if isinstance(node, ast.ClassDef):  # objet Meta
            if node.name == "Meta":
                class_meta = node
        elif isinstance(node, ast.Assign):  # Champs du modèle
            if len(node.targets) != 1:
                continue

            if not isinstance(node.targets[0], ast.Name):
                continue

            field_name = node.targets[0].id
            try:
                field = model._meta.get_field(field_name)
            except FieldDoesNotExist:
                continue

            for message in check_verbose_name(field, node):
                yield message

    if class_meta is None:
        yield django.core.checks.Warning(
            "Le modèle n’a pas de Meta.",
            hint="Ajouter un objet Meta.",
            obj=model,
            id="anybox.W001",
        )

    else:
        verbose_name = None
        verbose_name_plural = None

        for node in ast.iter_child_nodes(class_meta):
            if not isinstance(node, ast.Assign):
                continue

            if not isinstance(node.targets[0], ast.Name):
                continue

            attr = node.targets[0].id

            if attr == "verbose_name":
                verbose_name = node

            if attr == "verbose_name_plural":
                verbose_name_plural = node

        if verbose_name is None:
            yield django.core.checks.Warning(
                "Le modèle n’a pas de nom verbeux.",
                hint="Ajouter un verbose_name.",
                obj=model,
                id="anybox.E004",
            )

        elif not is_gettext_node(verbose_name):
            yield django.core.checks.Warning(
                "Le nom verbeux du modèle n’utilise pas gettext.",
                hint="Ajouter _() autour du verbose_name actuel.",
                obj=model,
                id="anybox.E005",
            )

        if verbose_name_plural is None:
            yield django.core.checks.Warning(
                "Le modèle n’a pas de nom verbeux pluriel.",
                hint="Ajouter un verbose_name_plural au Meta du modèle.",
                obj=model,
                id="anybox.E006",
            )

        elif not is_gettext_node(verbose_name_plural):
            yield django.core.checks.Warning(
                "Le nom verbeux pluriel du modèle n’utilise pas gettext.",
                hint="Ajouter _() autour du verbose_name_plural actuel.",
                obj=model,
                id="anybox.E007",
            )


@django.core.checks.register(django.core.checks.Tags.models)
def check_models(
    app_configs: django.apps.AppConfig, **kwargs
) -> Iterable[django.core.checks.CheckMessage]:
    """Vérification des modèles du projet."""
    errors = []
    for app in django.apps.apps.get_app_configs():
        if app.path.find("site-packages") > -1:  # on ignore les paquets tiers.
            continue

        for model in app.get_models():
            for check_message in check_model(model):
                errors.append(check_message)

    return errors
