# Generated by Django 4.1.2 on 2022-11-03 14:02

import core.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import django_countries.fields
import phonenumber_field.modelfields
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
    ]

    operations = [
        migrations.CreateModel(
            name="User",
            fields=[
                ("password", models.CharField(max_length=128, verbose_name="password")),
                (
                    "last_login",
                    models.DateTimeField(
                        blank=True, null=True, verbose_name="last login"
                    ),
                ),
                (
                    "is_superuser",
                    models.BooleanField(
                        default=False,
                        help_text="Designates that this user has all permissions without explicitly assigning them.",
                        verbose_name="superuser status",
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        blank=True, max_length=150, verbose_name="first name"
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        blank=True, max_length=150, verbose_name="last name"
                    ),
                ),
                (
                    "is_staff",
                    models.BooleanField(
                        default=False,
                        help_text="Designates whether the user can log into this admin site.",
                        verbose_name="staff status",
                    ),
                ),
                (
                    "is_active",
                    models.BooleanField(
                        default=True,
                        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
                        verbose_name="active",
                    ),
                ),
                (
                    "date_joined",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="date joined"
                    ),
                ),
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        max_length=254, unique=True, verbose_name="courriel"
                    ),
                ),
                (
                    "picture",
                    models.FileField(
                        blank=True,
                        upload_to=core.models.picture_path,
                        verbose_name="photo",
                    ),
                ),
                (
                    "consent_date",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="consentement donné le"
                    ),
                ),
                (
                    "phone",
                    phonenumber_field.modelfields.PhoneNumberField(
                        blank=True,
                        max_length=128,
                        null=True,
                        region=None,
                        verbose_name="téléphone",
                    ),
                ),
                (
                    "address",
                    models.TextField(blank=True, null=True, verbose_name="adresse"),
                ),
                (
                    "postcode",
                    models.CharField(
                        blank=True, max_length=10, null=True, verbose_name="code postal"
                    ),
                ),
                (
                    "town",
                    models.CharField(
                        blank=True, max_length=200, null=True, verbose_name="ville"
                    ),
                ),
                (
                    "country",
                    django_countries.fields.CountryField(
                        default="FR", max_length=2, verbose_name="pays"
                    ),
                ),
                (
                    "groups",
                    models.ManyToManyField(
                        blank=True,
                        help_text="The groups this user belongs to. A user will get all permissions granted to each of their groups.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.group",
                        verbose_name="groups",
                    ),
                ),
                (
                    "user_permissions",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Specific permissions for this user.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.permission",
                        verbose_name="user permissions",
                    ),
                ),
            ],
            options={
                "verbose_name": "utilisateur",
                "verbose_name_plural": "utilisateurs",
            },
            managers=[
                ("objects", core.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name="Membership",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "category",
                    models.CharField(
                        choices=[("member", "Membre"), ("admin", "Administrateur")],
                        default="member",
                        max_length=20,
                        verbose_name="catégorie",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(auto_now_add=True, verbose_name="création"),
                ),
                (
                    "updated_at",
                    models.DateTimeField(auto_now=True, verbose_name="mise à jour"),
                ),
            ],
            options={
                "verbose_name": "membre",
                "verbose_name_plural": "membres",
            },
        ),
        migrations.CreateModel(
            name="Organisation",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("name", models.TextField(verbose_name="nom")),
                (
                    "code",
                    models.CharField(
                        help_text="SIREN pour les entreprises FR",
                        max_length=30,
                        verbose_name="code",
                    ),
                ),
                (
                    "country",
                    django_countries.fields.CountryField(
                        max_length=2, verbose_name="pays"
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(auto_now_add=True, verbose_name="création"),
                ),
                (
                    "updated_at",
                    models.DateTimeField(auto_now=True, verbose_name="mise à jour"),
                ),
                (
                    "members",
                    models.ManyToManyField(
                        through="core.Membership",
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="membres",
                    ),
                ),
            ],
            options={
                "verbose_name": "organisation",
                "verbose_name_plural": "organisations",
            },
        ),
        migrations.AddField(
            model_name="membership",
            name="organisation",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to="core.organisation",
                verbose_name="organisation",
            ),
        ),
        migrations.AddField(
            model_name="membership",
            name="user",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="memberships",
                to=settings.AUTH_USER_MODEL,
                verbose_name="utilisateur",
            ),
        ),
        migrations.CreateModel(
            name="Actor",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        blank=True, max_length=200, null=True, verbose_name="prénom"
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        blank=True,
                        max_length=200,
                        null=True,
                        verbose_name="nom de famille",
                    ),
                ),
                (
                    "email",
                    models.EmailField(
                        blank=True, max_length=254, null=True, verbose_name="email"
                    ),
                ),
                (
                    "phone",
                    models.EmailField(
                        blank=True, max_length=254, null=True, verbose_name="téléphone"
                    ),
                ),
                (
                    "address",
                    models.TextField(blank=True, null=True, verbose_name="adresse"),
                ),
                (
                    "postcode",
                    models.CharField(
                        blank=True, max_length=10, null=True, verbose_name="code postal"
                    ),
                ),
                (
                    "town",
                    models.CharField(
                        blank=True, max_length=200, null=True, verbose_name="ville"
                    ),
                ),
                (
                    "country",
                    django_countries.fields.CountryField(
                        blank=True,
                        default="FR",
                        max_length=2,
                        null=True,
                        verbose_name="pays",
                    ),
                ),
                (
                    "company",
                    models.CharField(
                        blank=True, max_length=200, null=True, verbose_name="société"
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(auto_now_add=True, verbose_name="création"),
                ),
                (
                    "updated_at",
                    models.DateTimeField(auto_now=True, verbose_name="mise à jour"),
                ),
                (
                    "organisation",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="actors",
                        to="core.organisation",
                        verbose_name="organisation",
                    ),
                ),
                (
                    "user",
                    models.OneToOneField(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="identities",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "verbose_name": "acteur",
                "verbose_name_plural": "acteurs",
            },
        ),
        migrations.AddConstraint(
            model_name="organisation",
            constraint=models.UniqueConstraint(
                fields=("country", "code"), name="Code unique par pays"
            ),
        ),
        migrations.AddConstraint(
            model_name="membership",
            constraint=models.UniqueConstraint(
                fields=("user", "organisation"),
                name="Membre une seule fois par organisation",
            ),
        ),
    ]
