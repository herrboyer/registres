"""Tester les vérifications système.

TODO: compléter la couverture.

Note:
* difficile de tester l'erreur Meta manquant car on a besoin du app_label = "tests"
"""
import uuid

import pytest
from django.apps.registry import Apps
from django.db import models
from django.test.utils import isolate_apps
from django.utils.translation import gettext as _

from core.checks import check_model


@pytest.fixture
@isolate_apps("core")
def django_app():
    yield Apps(["core"])


@pytest.fixture
def fake_model_a(django_app):
    class FakeModelA(models.Model):
        """Un modèle avec des erreurs de gettext."""

        id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
        name = models.TextField(
            _("libellé avec gettext"), help_text="help_text sans gettext"
        )
        code = models.CharField(
            "libellé sans gettext", help_text=_("help_text"), max_length=30
        )
        # members = models.ManyToManyField("FakeModelB")
        # organisation = models.ForeignKey("FakeModelC", on_delete=models.CASCADE)

        class Meta:
            app_label = "tests"
            # manque verbose_name_plural
            verbose_name = _("FakeModelA")

        def __str__(self) -> str:
            return self.name

    yield FakeModelA


def obj_to_str(obj):
    """Rendu textuel des objets passés dans les checks.

    reprend le code de django.core.checks.messages
    """
    if obj is None:
        return "?"
    elif isinstance(obj, models.base.ModelBase):
        return obj._meta.label
    else:
        return str(obj)


def test_check_model_a(fake_model_a):
    messages = [m for m in check_model(fake_model_a)]
    assert len(messages) == 3
    error_tuples = [(obj_to_str(m.obj), m.id) for m in messages]
    expected = [
        ("tests.FakeModelA.name", "anybox.E003"),
        ("tests.FakeModelA.code", "anybox.E002"),
        ("tests.FakeModelA", "anybox.E006"),
    ]
    for error in expected:
        assert error in error_tuples
