"""Test des vues."""
from http import HTTPStatus

from django.urls import reverse
from pytest_django.asserts import assertTemplateUsed


def test_home(client):
    url = reverse("home")
    response = client.get(url)
    assert response.status_code == HTTPStatus.OK
    assertTemplateUsed(response, "home.html")


def test_login(client):
    url = reverse("login")
    response = client.get(url)
    assert response.status_code == HTTPStatus.OK
    assertTemplateUsed(response, "registration/login.html")
    assert b"csrfmiddlewaretoken" in response.content


def test_dashboard_redirect_not_logged_in(client):
    url = reverse("dashboard-redirect")
    response = client.get(url, follow=True)
    assert len(response.redirect_chain) == 1
    redirect_url = reverse("login") + "?next=/dashboard"
    # redirect_chain est une liste de tuple (url, code)
    assert response.redirect_chain[0][0] == redirect_url
    assertTemplateUsed(response, "registration/login.html")


def test_dashboard_redirect_logged_in(auth_client, default_organisation):
    url = reverse("dashboard-redirect")
    response = auth_client.get(url, follow=True)
    assert len(response.redirect_chain) == 1
    redirect_url = reverse("dashboard", args=[default_organisation.pk])
    # redirect_chain est une liste de tuple (url, code)
    assert response.redirect_chain[0][0] == redirect_url
    assertTemplateUsed(response, "dashboard.html")
