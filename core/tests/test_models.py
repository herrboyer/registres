"""Tests modèles."""
import pytest

from core.models import get_sentinel_user

pytestmark = [pytest.mark.django_db]


def test_user_str(user_factory):
    user = user_factory.create(
        first_name="Any", last_name="Box", email="registres@anybox.fr"
    )
    assert str(user) == "Any Box <registres@anybox.fr>"


@pytest.mark.parametrize(
    "email",
    ["", None, False],
)
def test_user_email_required(user_factory, email):
    with pytest.raises(ValueError):
        user_factory(email=email)


def test_user_auth_case_insensitive(user, client):
    assert client.login(email=user.email, password="testeur_registres")
    assert client.login(email=user.email.upper(), password="testeur_registres")


def test_superuser(superuser):
    assert superuser.is_superuser


def test_sentinel_user_email(settings):
    sentinel = get_sentinel_user()
    assert sentinel.email == settings.SENTINEL_USER_EMAIL


def test_sentinel_user_unique():
    first = get_sentinel_user()
    second = get_sentinel_user()
    assert first == second


def test_organisation_str(organisation_factory):
    organisation = organisation_factory.create(name="Annie Box")
    assert str(organisation) == "Annie Box"
