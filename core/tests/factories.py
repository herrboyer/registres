"""Générateurs pour les core models."""
from typing import Optional

import factory

from core import models

DEFAULT_PASSWORD = "testeur_registres"


class UserFactory(factory.django.DjangoModelFactory):
    """Générateur d'utilisateur.

    Chaque utilisateur aura comme mot de passe testeur_registres.
    """

    class Meta:
        model = models.User

    first_name = factory.Faker("first_name", locale="fr_FR")
    last_name = factory.Faker("last_name", locale="fr_FR")
    email = factory.Faker("company_email", locale="fr_FR")

    address = factory.Faker("street_address", locale="fr_FR")
    postcode = factory.Faker("postcode", locale="fr_FR")
    town = factory.Faker("city", locale="fr_FR")
    country = "FR"

    @factory.post_generation
    def password(obj: models.User, create: bool, extracted: Optional[str], **kwargs):
        """Crée un mot de passe pour l'utilisateur."""
        if not create:
            return
        obj.set_password(extracted or DEFAULT_PASSWORD)
        obj.save()
        return obj

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """Utilisation du manager pour créer un utilisateur de base."""
        manager = cls._get_manager(model_class)
        return manager.create_user(**kwargs)


class SuperuserFactory(UserFactory):
    """Générateur d'administrateur."""

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """Utilisation du manager pour créer un admin."""
        manager = cls._get_manager(model_class)
        return manager.create_superuser(**kwargs)


class OrganisationFactory(factory.django.DjangoModelFactory):
    """Générateur d'Organisation."""

    class Meta:
        model = models.Organisation

    name = factory.Faker("company", locale="fr_FR")
    code = factory.Faker("siren", locale="fr_FR")
    country = "FR"

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """Utilisation du manager pour créer une organisation complète.

        Important de passer un kwarg user.
        """
        manager = cls._get_manager(model_class)
        return manager.create_with_fixtures(**kwargs)


class MembershipFactory(factory.django.DjangoModelFactory):
    """Générateur d'appartenance à une organisation."""

    class Meta:
        model = models.Membership

    organisation = factory.SubFactory(OrganisationFactory)
    user = factory.SubFactory(UserFactory)

    class Params:
        """Traits.

        * is_admin=True: ``user`` sera administrateur d'``organisation``
        """

        is_admin = factory.Trait(category=models.Membership.Categories.ADMIN)


class ActorFactory(factory.django.DjangoModelFactory):
    """Générateur d'Actor."""

    class Meta:
        model = models.Actor

    first_name = factory.Faker("first_name", locale="fr_FR")
    last_name = factory.Faker("last_name", locale="fr_FR")
    company = factory.Faker("company", locale="fr_FR")
    address = factory.Faker("street_address", locale="fr_FR")
    postcode = factory.Faker("postcode", locale="fr_FR")
    town = factory.Faker("city", locale="fr_FR")
    country = "FR"
    email = factory.Faker("company_email", locale="fr_FR")
    phone = factory.Faker("phone_number", locale="fr_FR")
    organisation = factory.SubFactory(OrganisationFactory)
