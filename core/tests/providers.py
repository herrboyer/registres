"""Fournisseurs de données Faker."""

from faker.providers import BaseProvider


class RegisterSentenceProvider(BaseProvider):
    """Des phrases types pour les registres."""

    data_whats = [
        "adresse",
        "taille",
        "pointure",
        "circonférence",
        "couleur",
        "texture",
        "profil",
        "numéro",
    ]

    articles = ["de", "du"]

    data_ofs = [
        "téléphone",
        "visage",
        "main",
        "siège",
        "lunettes",
        "pantalon",
        "compte en banque",
        "sécurité sociale",
        "cheveux",
    ]

    security_actions = [
        "chiffrement",
        "destruction",
        "installation",
        "blindage",
        "mime",
        "cachage",
        "dissimulation",
        "audit",
        "reconnaissance",
        "armement",
    ]

    security_ofs = [
        "mots de passe",
        "faciale",
        "empreintes digitales",
        "pièges",
        "feintes",
        "tiroir",
        "coffre",
        "clé",
        "photos",
        "IA",
    ]

    people_types = [
        "gens",
        "internautes",
        "ennemis",
        "clients",
        "amis",
        "collègues",
        "dirigeants",
        "décideurs",
        "entreprenautes",
    ]

    people_qualifiers = [
        "loyaux",
        "jurés",
        "sobres",
        "inconnus",
        "libristes",
        "tête en l'air",
        "abstraits",
        "gagnants",
    ]

    goal_actions = ["gagner", "faire", "prendre", "éliminer", "perdre", "salir"]
    goal_ofs = ["l'argent", "business", "concurrent", "poids", "bonheur", "la tête"]

    processing_whats = [
        "newsletter",
        "fichier",
        "bulletins",
        "base",
        "suivi",
        "fichage",
        "liste noire",
    ]

    processing_ofs = [
        "abonnés",
        "clients",
        "salariés",
        "concurrents",
        "de données",
        "de paye",
        "adhérents",
        "mauvais payeurs",
    ]

    recipient_whats = [
        "régie",
        "hébergeur",
        "service",
    ]

    recipient_ofs = [
        "administratif",
        "web",
        "publicitaire",
        "client",
        "abonnés",
        "adhérents",
        "après-vente",
        "impayés",
    ]

    def data(self):
        """Catégorie de donnée.

        ex: adresse du pantalon, numéro de sécurité sociale.
        """
        return " ".join(
            [
                self.random_element(self.data_whats),
                self.random_element(self.articles),
                self.random_element(self.data_ofs),
            ]
        )

    def security(self):
        """Mesure de sécurité.

        ex: chiffrement de mots de passe, armement du tiroir.
        """
        return " ".join(
            [
                self.random_element(self.security_actions),
                self.random_element(self.articles),
                self.random_element(self.security_ofs),
            ]
        )

    def people(self):
        """Catégorie de personne.

        ex: ennemis tête en l'air, collègues libristes.
        """
        return " ".join(
            [
                self.random_element(self.people_types),
                self.random_element(self.people_qualifiers),
            ]
        )

    def goal(self):
        """Objectif.

        ex: prendre du poids, faire du business.
        """
        return " ".join(
            [
                self.random_element(self.people_types),
                self.random_element(self.people_qualifiers),
            ]
        )

    def processing(self):
        """Traitement.

        ex: newsletter adhérents, suivi salariés.
        """
        return " ".join(
            [
                self.random_element(self.processing_whats),
                self.random_element(self.processing_ofs),
            ]
        )

    def recipient(self):
        """Destinataire.

        ex: service client, régie publicitaire.
        """
        return " ".join(
            [
                self.random_element(self.recipient_whats),
                self.random_element(self.recipient_ofs),
            ]
        )
