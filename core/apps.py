"""Core config.

À l'initialisation de l'app on a besoin de peupler la base avec une organisation par
défaut et d'utilisateurs.
* On pourrait passer par une ``command`` mais ça serait pas forcement hyper simple à
  lancer à chaque déploiement.
* On pourrait faire une migration de données, mais ça fait des mauvaises surprises lors
  d'un nouveau déploiement quand le modèle a changé.
* On tente de le faire ici avec un signal post_migrate !


On récupère les valeurs dans les settings.

Aussi, pour uniformiser le code dans le projet et ne pas perdre de temps dans les revues
on charge plusieurs vérifications système.
"""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CoreConfig(AppConfig):
    """Config."""

    name = "core"
    verbose_name = _("cœur")

    def ready(self):
        """Démarrage de core.

        * Connecte le signal `post_migrate` à l'initialisation des données
        * Charge les vérifications système
        """
        import core.checks  # noqa: F401
        import core.signals  # noqa: F401
