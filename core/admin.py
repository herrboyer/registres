"""Administration du cœur de l'app."""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

from core.models import Actor, Membership, Organisation, User

# à ce stade on utilise pas les groupes django
admin.site.unregister(Group)


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    """Administration des utilisateurs."""

    list_display = [
        "email",
        "first_name",
        "last_name",
        "phone",
        "is_active",
    ]
    list_filter = ["is_active", "date_joined"]
    search_fields = ["first_name", "last_name", "email", "phone"]
    ordering = ["last_name"]
    readonly_fields = ("consent_date", "last_login", "date_joined")
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            _("Contact"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "phone",
                    "picture",
                )
            },
        ),
        (_("Droits"), {"fields": ("is_active", "is_superuser")}),
        (
            _("Dates importantes"),
            {"fields": ("last_login", "date_joined", "consent_date")},
        ),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2")}),
    )


class MembershipInline(admin.TabularInline):
    """Ajout de membres depuis l'édition d'une organisation."""

    model = Membership
    autocomplete_fields = ["user"]


@admin.register(Organisation)
class OrganisationAdmin(admin.ModelAdmin):
    """Gestion des organisations."""

    list_display = (
        "name",
        "country",
        "code",
    )
    search_fields = ["name", "code"]
    ordering = ["country", "name"]
    list_filter = ("country", "created_at")
    inlines = [MembershipInline]


@admin.register(Actor)
class ActorAdmin(admin.ModelAdmin):
    """Gestion des acteurs."""

    ordering = ["last_name"]
    list_display = (
        "last_name",
        "first_name",
        "company",
        "email",
        "phone",
        "user",
        "created_at",
        "updated_at",
    )
    search_fields = ["organisation", "last_name", "first_name", "company", "email"]
    list_filter = (
        "organisation",
        "created_at",
    )
    autocomplete_fields = ["organisation", "user"]
