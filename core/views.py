"""Vues."""

from django.views.generic import TemplateView, RedirectView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Organisation


class HomeView(TemplateView):
    """Page d'accueil."""

    template_name = "home.html"


class DashboardRedirectView(LoginRequiredMixin, RedirectView):
    """Redirection vers le tableau de bord de l'organisation par défaut."""

    pattern_name = "dashboard"

    def get_redirect_url(self, *args, **kwargs):
        """On récupère l'organisation par défaut en session."""
        return super().get_redirect_url(self.request.session["organisation_id"])


class DashboardView(LoginRequiredMixin, DetailView):
    """Tableau de bord d'une organisation."""

    template_name = "dashboard.html"
    model = Organisation
    context_object_name = "organisation"
    pk_url_kwarg = "organisation_pk"

    subtitle = "Résumé de l'activité"

    def title(self) -> str:
        """Titre."""
        return f"Tableau de bord {self.object}"
