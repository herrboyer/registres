"""Modèles du cœur de l'app."""
import uuid
from pathlib import Path
from typing import List, Optional

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import gettext as _
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

from core.helpers import media_relative_path, mkdir_p
from registers import fixtures


def get_sentinel_user():
    """L'utilisateur « sentinel » remplace les utilisateurs en cas de suppression.

    Il est appelé lors des on_delete sur les modèles liés à User.
    """
    return get_user_model().objects.get_or_create(email=settings.SENTINEL_USER_EMAIL)[0]


def picture_path(instance: "User", filename: str) -> Path:
    """Renomme le ``filename`` en ``instance.slug``.``ext``.

    Retourne le chemin complet souhaité pour le fichier.
    """
    suffix = Path(filename).suffix

    mkdir_p(settings.PICTURES_PATH / "profil")
    return media_relative_path(
        settings.PICTURES_PATH / "profil" / f"{instance.pk}{suffix}"
    )


class UserManager(BaseUserManager):
    """Manager pour User sans username."""

    use_in_migrations = True

    def _create_user(
        self,
        email: str,
        password: Optional[str],
        is_staff: bool,
        is_superuser: bool,
        **extra_fields,
    ) -> "User":
        """Création de l'utilisateur sans username."""
        if not email:
            raise ValueError(_("Email requis"))
        # nique la RFC 5321 et particulièrement le point 2.4 paragraphe 2
        email = email.lower()
        user = self.model(
            email=email, is_staff=is_staff, is_superuser=is_superuser, **extra_fields
        )
        user.set_password(password)
        user.save()
        return user

    def create_user(self, email: str, password: str = None, **extra_fields) -> "User":
        """Nouveau ``User`` simple sans username."""
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(
        self, email: str, password: str = None, **extra_fields
    ) -> "User":
        """Nouveau ``SuperUser`` sans username."""
        return self._create_user(email, password, True, True, **extra_fields)

    def get_by_natural_key(self, email: Optional[str]) -> "User":
        """Email insensible à la casse pour l'authentification."""
        return self.get(email__iexact=email)


class User(AbstractUser):
    """Utilisateur sans username mais avec un email."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    username = None
    email = models.EmailField(_("courriel"), unique=True)
    picture = models.FileField(  # TODO: solr-thumbnail ou autre
        _("photo"), upload_to=picture_path, blank=True, null=False
    )
    consent_date = models.DateTimeField(_("consentement donné le"), auto_now_add=True)
    phone = PhoneNumberField(_("téléphone"), blank=True, null=True)

    address = models.TextField(_("adresse"), null=True, blank=True)
    postcode = models.CharField(_("code postal"), max_length=10, null=True, blank=True)
    town = models.CharField(_("ville"), max_length=200, null=True, blank=True)
    country = CountryField(_("pays"), default="FR")

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS: List[Optional[str]] = ["last_name", "first_name"]
    EMAIL_FIELD = USERNAME_FIELD

    objects = UserManager()

    class Meta:
        verbose_name = _("utilisateur")
        verbose_name_plural = _("utilisateurs")

    def save(self, *args, **kwargs):
        """On sauve une première fois sans l'image pour acceder à la pk, slug, etc."""
        if not self.pk:
            picture_ = self.picture
            self.picture = None
            super().save(*args, **kwargs)
            self.picture = picture_
            super().save(update_fields=["picture"])
        else:
            super().save(*args, **kwargs)

    def __str__(self) -> str:
        return self.email_to

    @property
    def email_to(self) -> str:
        """Adresse mail avec nom."""
        return f"{self.full_name} <{self.email}>"

    @property
    def full_name(self) -> str:
        """Nom complet."""
        return f"{self.first_name} {self.last_name}"


class OrganisationManager(models.Manager):
    """Gestionnaire pour les organisations."""

    def create_with_fixtures(self, *args, **kwargs):
        """Création des données par défaut pour les nouvelles organisations.

        On crée les catégories de données, personnes, etc. par défaut.

        Il est utile de passer ``user`` en kwarg de cette méthode pour créer les
        registres par défaut de l'organisation en spécifiant cet utilisateur comme
        responsable.
        """
        from registers.models import (
            ControllerRegister,
            ProcessorRegister,
            BreachRegister,
            RequestRegister,
        )

        user = kwargs.pop("user", None)
        organisation = self.model(*args, **kwargs)
        organisation.save()
        for data in fixtures.DATAS:
            organisation.datas.create(**data)
        for data in fixtures.PEOPLES:
            organisation.peoples.create(**data)
        for data in fixtures.RECIPIENTS:
            organisation.recipients.create(**data)
        for data in fixtures.SECURITIES:
            organisation.securities.create(**data)
        if user:
            Membership.objects.get_or_create(
                user=user,
                organisation=organisation,
                category=Membership.Categories.ADMIN,
            )
            actor = Actor.objects.create(user=user, organisation=organisation)
            controller_register_name = _(
                "Registre des traitements %(organisation)s"
                % {"organisation": organisation}
            )
            ControllerRegister.objects.create(
                name=controller_register_name,
                representative=actor,
                organisation=organisation,
            )
            processor_register_name = _(
                "Registre du sous-traitant %(organisation)s"
                % {"organisation": organisation}
            )
            ProcessorRegister.objects.create(
                name=processor_register_name,
                representative=actor,
                organisation=organisation,
            )
            breach_register_name = _(
                "Registre des violations %(organisation)s"
                % {"organisation": organisation}
            )
            BreachRegister.objects.create(
                name=breach_register_name,
                representative=actor,
                organisation=organisation,
            )
            request_register_name = _(
                "Registre des demandes %(organisation)s"
                % {"organisation": organisation}
            )
            RequestRegister.objects.create(
                name=request_register_name,
                organisation=organisation,
            )
        return organisation


class Organisation(models.Model):
    """Une organisation regroupe des utilisateurs.

    Le modèle utilise un manager dédié ``OrganisationManager`` qui implémente une
    méthode ``create`` qui permet la création de données initales pour l'organisation.
    Pensez à utiliser ``Organisation.objects.create(name="nom", ...)`` pour en
    bénéficier.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(_("nom"))
    code = models.CharField(
        _("code"), help_text=_("SIREN pour les entreprises FR"), max_length=30
    )
    country = CountryField(_("pays"))
    members = models.ManyToManyField(
        get_user_model(), through="Membership", verbose_name=_("membres")
    )
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)

    objects = OrganisationManager()

    class Meta:
        verbose_name = _("organisation")
        verbose_name_plural = _("organisations")
        constraints = [
            models.UniqueConstraint(
                fields=["country", "code"], name=_("Code unique par pays")
            )
        ]

    def __str__(self) -> str:
        return self.name


class Membership(models.Model):
    """Lien entre un utilisateur et une organisation.

    Permet de définir la catégorie de l'utilisateur.
    """

    class Categories(models.TextChoices):
        """Enum catégories de membre."""

        MEMBER = "member", _("Membre")
        ADMIN = "admin", _("Administrateur")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organisation = models.ForeignKey(
        Organisation, on_delete=models.CASCADE, verbose_name=_("organisation")
    )
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="memberships",
        verbose_name=_("utilisateur"),
    )
    category = models.CharField(
        _("catégorie"),
        choices=Categories.choices,
        default=Categories.MEMBER,
        max_length=20,
    )
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)

    class Meta:
        verbose_name = _("membre")
        verbose_name_plural = _("membres")
        constraints = [
            models.UniqueConstraint(
                fields=["user", "organisation"],
                name=_("Membre une seule fois par organisation"),
            )
        ]


class Actor(models.Model):
    """Un acteur est une personne responsable.

    La différence entre un acteur et un utilisateur c'est que l'acteur n'a pas forcément
    de compte utilisateur sur la plateforme.

    Il peut être par contre lié à un compte utilisateur, dans ce cas les champs nom,
    prénom, etc. ne sont plus requis.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    first_name = models.CharField(_("prénom"), max_length=200, null=True, blank=True)
    last_name = models.CharField(
        _("nom de famille"), max_length=200, null=True, blank=True
    )
    email = models.EmailField(_("email"), null=True, blank=True)
    phone = models.EmailField(_("téléphone"), null=True, blank=True)

    address = models.TextField(_("adresse"), null=True, blank=True)
    postcode = models.CharField(_("code postal"), max_length=10, null=True, blank=True)
    town = models.CharField(_("ville"), max_length=200, null=True, blank=True)
    country = CountryField(_("pays"), default="FR", null=True, blank=True)

    organisation = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name="actors",
        verbose_name=_("organisation"),
    )
    company = models.CharField(_("société"), max_length=200, null=True, blank=True)
    user = models.OneToOneField(
        User,
        on_delete=models.SET_NULL,
        related_name="identities",
        null=True,
        blank=True,
    )
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)

    class Meta:
        verbose_name = _("acteur")
        verbose_name_plural = _("acteurs")

    @property
    def company_name(self) -> str:
        """Société."""
        if self.company:
            return self.company
        else:
            return str(self.organisation)

    @property
    def full_name(self) -> str:
        """Nom complet."""
        name = " ".join([self.first_name or "", self.last_name or ""])
        return name.strip()

    def __str__(self) -> str:
        fields = [self.full_name]
        if self.company:
            fields.append(self.company)
        return ", ".join(fields)

    def save(self, *args, **kwargs):
        """Si on dispose d'un user, on utilise les données de son profil si besoin."""
        if self.user:
            self.first_name = self.first_name or self.user.first_name
            self.last_name = self.last_name or self.user.last_name
            self.email = self.email or self.user.email
            self.phone = self.phone or self.user.phone
            self.address = self.address or self.user.address
            self.postcode = self.postcode or self.user.postcode
            self.town = self.town or self.user.town
            self.country = self.country or self.user.country

        super().save(*args, **kwargs)
