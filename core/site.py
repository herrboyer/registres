"""Administration des modules de base et personnalisation de l'admin site."""
from django.contrib import admin
from django.contrib.admin.apps import AdminConfig
from django.utils.translation import gettext_lazy as _


class CoreAdminConfig(AdminConfig):
    """Personnalisation de l'admin."""

    default_site = "core.site.RegistresAdminSite"


class RegistresAdminSite(admin.AdminSite):
    """Personnalisation de l'admin django."""

    site_header = _("Registres")
    site_title = _("Gestion des registres")
