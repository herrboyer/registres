"""Boîte à outils."""
from pathlib import Path

from django.conf import settings


def mkdir_p(path: str):
    """Crée, si besoin, le ``path`` spécifié.

    Utile dans les fonctions de déplacement de fichiers téléversés.
    """
    Path(path).mkdir(parents=True, exist_ok=True)


def media_relative_path(path: Path) -> Path:
    """Retourne le chemin d'un media relatif à settings.MEDIA_ROOT.

    Ex.
    MEDIA_ROOT = "/home"
    media_relative_path("/home/images/top.jpg") -> images/top.jpg

    Utile dans les fonctions de déplacement de fichiers téléversés.
    """
    return path.relative_to(settings.MEDIA_ROOT)
