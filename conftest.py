"""Fixtures pytest.

Plusieurs greffons pytest sont installé, pensez à eux quand vous nommez vos fixtures
(particulièrement à pytest-factoryboy qui créé, par exemple, user pour la UserFactory).
"""
import pytest
from django.contrib.auth import get_user_model
from pytest_factoryboy import register

from core.tests.factories import (
    ActorFactory,
    MembershipFactory,
    OrganisationFactory,
    SuperuserFactory,
    UserFactory,
)
from registers.tests.factories import (
    ControllerRegisterFactory,
    CustomerFactory,
    DataFactory,
    PeopleFactory,
    ProcessingCategoryFactory,
    ProcessingFactory,
    ProcessorRegisterFactory,
    RecipientFactory,
    SecurityFactory,
)

# Core
register(MembershipFactory)
register(OrganisationFactory)
register(UserFactory)
register(SuperuserFactory, "superuser")
register(ActorFactory)

# Registers
register(DataFactory)
register(PeopleFactory)
register(RecipientFactory)
register(SecurityFactory)

# Controller Registers
register(ControllerRegisterFactory)
register(ProcessingFactory)

# Processor Registers
register(CustomerFactory)
register(ProcessingCategoryFactory)
register(ProcessorRegisterFactory)


TEST_USER_EMAIL = "user@domain.fr"
TEST_PASSWORD = "passe hou heure de"
TEST_ORGANISATION_NAME = "Anycrate"


@pytest.fixture
def default_user(db):
    """Un utilisateur de test."""
    user_model = get_user_model()
    user = user_model.objects.filter(email=TEST_USER_EMAIL).first()
    if not user:
        user = UserFactory(email=TEST_USER_EMAIL, password=TEST_PASSWORD)
    return user


@pytest.fixture
def default_organisation(db, default_user):
    """Organisation de test."""
    organisation = OrganisationFactory(name=TEST_ORGANISATION_NAME, user=default_user)
    return organisation


@pytest.fixture
def admin_user(db, default_user, default_organisation):
    """Retourne default_user cette fois ci administrateur de default_organisation."""
    MembershipFactory(
        user=default_user, organisation=default_organisation, is_admin=True
    )
    return default_user


@pytest.fixture(scope="function")
def auth_client(client, default_user, default_organisation):
    """Un TestClient authentifié."""
    client.login(username=default_user.email, password=TEST_PASSWORD)
    return client
