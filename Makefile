info: header
define HEADER

██████╗ ███████╗ ██████╗ ██╗███████╗████████╗██████╗ ███████╗███████╗
██╔══██╗██╔════╝██╔════╝ ██║██╔════╝╚══██╔══╝██╔══██╗██╔════╝██╔════╝
██████╔╝█████╗  ██║  ███╗██║███████╗   ██║   ██████╔╝█████╗  ███████╗
██╔══██╗██╔══╝  ██║   ██║██║╚════██║   ██║   ██╔══██╗██╔══╝  ╚════██║
██║  ██║███████╗╚██████╔╝██║███████║   ██║   ██║  ██║███████╗███████║
╚═╝  ╚═╝╚══════╝ ╚═════╝ ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝

endef
export HEADER

# Par défaut on execute les commandes dans avec le docker-compose de dev.
# Pour faire (par exemple) un `run` en prod :
# DOCKER_COMPOSE_CMD="docker-compose -f docker-compose.yml" make run
ifeq ($(DOCKER_COMPOSE_CMD),)
DOCKER_COMPOSE_CMD := docker-compose -f docker-compose.yml -f docker-compose.dev.yml
endif

build: ## construction des images
	$(DOCKER_COMPOSE_CMD) build

check: ## Vérifications système Django, passer arguments avec args="app"
	$(DOCKER_COMPOSE_CMD) run app python manage.py check $(args)

clean: ## nettoyage des fichiers de dev
	$(DOCKER_COMPOSE_CMD) run app \
		rm -rf public .cache/coverage .cache/*/ report.xml logs/* 
	$(DOCKER_COMPOSE_CMD) run --rm app \
		find . -type d -name __pycache__ -exec rm -r {} \+

documentation: ## génération de la documentation sphinx dans docs/
	$(DOCKER_COMPOSE_CMD) run --rm app sphinx-build -b html docs public

migrate: ## execution des migrations
	$(DOCKER_COMPOSE_CMD) run --rm migrate

migrations: ## génération des migrations, passer arguments avec args="app"
	$(DOCKER_COMPOSE_CMD) run --rm app python manage.py makemigrations $(args)

requirements: ## mise à jour des dépendances (pip-compile)
	$(DOCKER_COMPOSE_CMD) run app \
		pip-compile --generate-hashes --verbose --upgrade \
		requirements.txt pyproject.toml
	$(DOCKER_COMPOSE_CMD) run --rm app \
		pip-compile --generate-hashes --verbose --upgrade --extra=dev \
		--output-file=requirements.dev.txt pyproject.toml

run: ## démarrage docker-compose
	$(DOCKER_COMPOSE_CMD) up

down: ## stop docker-compose avec --remove-orphans
	$(DOCKER_COMPOSE_CMD) down --remove-orphans

shell: ## shell django
	$(DOCKER_COMPOSE_CMD) run --rm app python manage.py shell

unit_tests: ## tests unitaires (pytest), passer arguments avec args="--lf"
	$(DOCKER_COMPOSE_CMD) run --rm app pytest $(args)

pull: ## récupération des images
	$(DOCKER_COMPOSE_CMD) pull

help:
	@echo "$$HEADER"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: all
.DEFAULT_GOAL := help
