# à dupliquer à la racine du projet en .env
ALLOWED_HOSTS=*,
DJANGO_DEBUG=on
SECRET_KEY=secret
POSTGRES_PASSWORD=secret
POSTGRES_USER=postgres
POSTGRES_DB=postgres
DEFAULT_USER_PASSWORD=taupe_secret
CSRF_TRUSTED_ORIGINS=http://registres.localhost:8080

# ne pas modifier (sauf si vous savez ce que vous faites, hein)
CUSTOM_COMPILE_COMMAND=make requirements
