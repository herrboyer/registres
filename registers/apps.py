"""Configuration de l'app registers."""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class RegistersConfig(AppConfig):
    """Registres."""

    name = "registers"
    verbose_name = _("registres")
