"""Données initiales.

Des listes de dictionnaires qui peuvent être passés aux modèles lors de la création d'une
nouvelle organisation.
"""
from django.utils.translation import gettext as _

DATAS = [
    {
        "name": _("État civil, identité, données d'identification, images…"),
        "sensitive": False,
    },
    {
        "name": _("Vie personnelle"),
        "description": _("Habitudes de vie, situation familiale, etc."),
        "sensitive": False,
    },
    {
        "name": _("Informations d'ordre économique et financier"),
        "description": _("Revenus, situation financière, situation fiscale, etc."),
        "sensitive": False,
    },
    {
        "name": _("Données de connexion"),
        "description": _("Adresse IP, logs, etc."),
        "sensitive": False,
    },
    {
        "name": _("Données de localisation"),
        "description": _("Déplacements, données GPS, GSM, etc."),
        "sensitive": False,
    },
    {"name": _("Numéro de Sécurité Sociale ou NIR"), "sensitive": False},
    {"name": _("Données révélant l'origine raciale ou ethnique"), "sensitive": True},
    {"name": _("Données révélant les opinions politiques"), "sensitive": True},
    {
        "name": _("Données révélant les convictions religieuses ou philosophiques"),
        "sensitive": True,
    },
    {"name": _("Données révélant l'appartenance syndicale"), "sensitive": True},
    {"name": _("Données génétiques"), "sensitive": True},
    {
        "name": _(
            "Données biométriques aux fins d'identifier une personne physique de manière unique"
        ),
        "sensitive": True,
    },
    {"name": _("Données concernant la santé"), "sensitive": True},
    {
        "name": _("Données concernant la vie sexuelle ou l'orientation sexuelle"),
        "sensitive": True,
    },
    {
        "name": _("Données relatives à des condamnations pénales ou infractions"),
        "sensitive": True,
    },
]


PEOPLES = [
    {"name": _("Salariés")},
    {"name": _("Services Internes")},
    {"name": _("Clients")},
    {"name": _("Fournisseurs")},
    {"name": _("Prestataires")},
    {"name": _("Prospects")},
    {"name": _("Candidats")},
]

RECIPIENTS = [
    {"name": _("Service interne qui traite les données")},
    {"name": _("Sous-traitants")},
    {"name": _("Destinataires dans des pays tiers ou organisations internationales")},
    {"name": _("Partenaires institutionnels ou commerciaux")},
]

SECURITIES = [
    {"name": _("Mesures de traçabilité")},
    {"name": _("Mesures de protection des logiciels")},
    {"name": _("Sauvegarde des données")},
    {"name": _("Chiffrement des données")},
    {"name": _("Contrôle d'accès des utilisateurs")},
    {"name": _("Contrôle des sous-traitants")},
]
