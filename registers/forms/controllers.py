"""Formulaires registre des traitements."""

from django.forms import ModelForm, TextInput, inlineformset_factory
from registers.models import (
    Processing,
    ControllerRegister,
    ProcessingPeople,
    ProcessingData,
    ProcessingRecipient,
    ProcessingSecurity,
)


class ControllerRegisterForm(ModelForm):
    """Formulaire de modification d'un registre de traitement."""

    class Meta:
        model = ControllerRegister
        fields = ["name", "representative", "dpo"]
        widgets = {
            "name": TextInput,
        }


class ProcessingForm(ModelForm):
    """Formulaire d'un traitement."""

    class Meta:
        model = Processing
        fields = ["name", "in_charge", "goals"]
        widgets = {
            "name": TextInput,
        }


class ProcessingPeopleForm(ModelForm):
    """Formulaire des liens entre les personnes concernées et un traitement."""

    class Meta:
        model = ProcessingPeople
        fields = ["people", "details"]


class ProcessingDataForm(ModelForm):
    """Formulaire des liens entre les données et un traitement."""

    class Meta:
        model = ProcessingData
        fields = ["data", "ttl", "details"]


class ProcessingRecipientForm(ModelForm):
    """Formulaire des liens entre les destinataires et un traitement."""

    class Meta:
        model = ProcessingRecipient
        fields = ["recipient", "details"]


class ProcessingSecurityForm(ModelForm):
    """Formulaire des liens entre les mesures de sécurité et un traitement."""

    class Meta:
        model = ProcessingSecurity
        fields = ["security", "details"]


ProcessingPeopleFormset = inlineformset_factory(
    Processing, ProcessingPeople, form=ProcessingPeopleForm, min_num=1, extra=1
)

ProcessingDataFormset = inlineformset_factory(
    Processing, ProcessingData, form=ProcessingDataForm, min_num=1, extra=1
)

ProcessingRecipientFormset = inlineformset_factory(
    Processing, ProcessingRecipient, form=ProcessingRecipientForm, min_num=1, extra=1
)

ProcessingSecurityFormset = inlineformset_factory(
    Processing, ProcessingSecurity, form=ProcessingSecurityForm, min_num=1, extra=1
)
