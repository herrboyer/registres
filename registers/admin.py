"""Interfaces d'admin des registres."""
from django.contrib import admin

from registers.models import (
    Breach,
    BreachRegister,
    ControllerRegister,
    Customer,
    Data,
    People,
    Processing,
    ProcessingCategory,
    ProcessingCategoryCustomer,
    ProcessingCategoryRecipient,
    ProcessingCategorySecurity,
    ProcessingData,
    ProcessingPeople,
    ProcessingRecipient,
    ProcessingSecurity,
    ProcessorRegister,
    Recipient,
    RequestRegister,
    Security,
)


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    """Gestion des clients."""

    list_display = (
        "name",
        "country",
        "code",
    )
    search_fields = (
        "name",
        "code",
    )
    ordering = (
        "country",
        "name",
    )
    list_filter = ("country", "created_at")


@admin.register(Data)
class DataAdmin(admin.ModelAdmin):
    """Gestion des catégories de données."""

    ordering = ("name",)
    list_display = ("name", "organisation", "sensitive", "created_at", "updated_at")
    search_fields = (
        "name",
        "description",
    )
    list_filter = ("organisation", "sensitive", "created_at")
    autocomplete_fields = ("organisation",)


@admin.register(Security)
class SecurityAdmin(admin.ModelAdmin):
    """Gestion des catégories de mesures de sécurité."""

    ordering = ("name",)
    list_display = ("name", "organisation", "created_at", "updated_at")
    search_fields = (
        "name",
        "description",
    )
    list_filter = (
        "organisation",
        "created_at",
    )
    autocomplete_fields = ("organisation",)


@admin.register(People)
class PeopleAdmin(admin.ModelAdmin):
    """Gestion des catégories de personnes."""

    list_display = ("name", "organisation", "created_at", "updated_at")
    search_fields = (
        "name",
        "description",
    )
    list_filter = (
        "organisation",
        "created_at",
    )
    autocomplete_fields = ("organisation",)


@admin.register(Recipient)
class RecipientAdmin(admin.ModelAdmin):
    """Gestion des destinataires."""

    list_display = (
        "name",
        "organisation",
        "category",
        "european",
        "created_at",
        "updated_at",
    )
    search_fields = (
        "name",
        "description",
    )
    list_filter = ("category", "european", "organisation", "created_at")
    autocomplete_fields = ("organisation",)


@admin.register(ControllerRegister)
class ControllerRegisterAdmin(admin.ModelAdmin):
    """Gestion des registres de traitement."""

    list_display = (
        "name",
        "organisation",
        "representative",
        "dpo",
        "created_at",
        "updated_at",
    )
    search_fields = ("name",)
    list_filter = ("created_at",)
    autocomplete_fields = (
        "representative",
        "dpo",
        "organisation",
    )


class ProcessingDataInline(admin.TabularInline):
    """Données des traitements."""

    model = ProcessingData
    fields = (
        "data",
        "description",
        "ttl",
    )
    autocomplete_fields = ("data",)
    extra = 1
    classes = ("collapse",)


class ProcessingPeopleInline(admin.TabularInline):
    """Personnes des traitements."""

    model = ProcessingPeople
    fields = (
        "people",
        "description",
    )
    autocomplete_fields = ("people",)
    extra = 1
    classes = ("collapse",)


class ProcessingRecipientInline(admin.TabularInline):
    """Destinataires des traitements."""

    model = ProcessingRecipient
    fields = (
        "recipient",
        "description",
    )
    autocomplete_fields = ("recipient",)
    extra = 1
    classes = ("collapse",)


class ProcessingSecurityInline(admin.TabularInline):
    """Mesures prises dans les traitements."""

    model = ProcessingSecurity
    fields = (
        "security",
        "description",
    )
    autocomplete_fields = ("security",)
    extra = 1
    classes = ("collapse",)


@admin.register(Processing)
class ProcessingAdmin(admin.ModelAdmin):
    """Traitements."""

    list_display = (
        "name",
        "register",
        "in_charge",
        "created_at",
        "updated_at",
    )
    search_fields = ("name",)
    list_filter = ("created_at",)
    autocomplete_fields = (
        "register",
        "in_charge",
    )
    inlines = [
        ProcessingPeopleInline,
        ProcessingRecipientInline,
        ProcessingDataInline,
        ProcessingSecurityInline,
    ]


@admin.register(ProcessorRegister)
class ProcessorRegisterAdmin(admin.ModelAdmin):
    """Gestion des registres de sous-traitance."""

    list_display = (
        "name",
        "organisation",
        "representative",
        "dpo",
        "created_at",
        "updated_at",
    )
    search_fields = ("name",)
    list_filter = ("created_at",)
    autocomplete_fields = (
        "representative",
        "dpo",
        "organisation",
    )


class ProcessingCategoryCustomerInline(admin.TabularInline):
    """Clients des catégories de traitement."""

    model = ProcessingCategoryCustomer
    fields = (
        "customer",
        "description",
    )
    autocomplete_fields = ("customer",)
    extra = 1
    classes = ("collapse",)


class ProcessingCategoryRecipientInline(admin.TabularInline):
    """Destinataires des catégories de traitement."""

    model = ProcessingCategoryRecipient
    fields = (
        "recipient",
        "description",
    )
    autocomplete_fields = ("recipient",)
    extra = 1
    classes = ("collapse",)


class ProcessingCategorySecurityInline(admin.TabularInline):
    """Mesures prises dans les catégories de traitement."""

    model = ProcessingCategorySecurity
    fields = (
        "security",
        "description",
    )
    autocomplete_fields = ("security",)
    extra = 1
    classes = ("collapse",)


@admin.register(ProcessingCategory)
class ProcessingCategoryAdmin(admin.ModelAdmin):
    """Catégories de traitement."""

    list_display = (
        "name",
        "register",
        "in_charge",
        "created_at",
        "updated_at",
    )
    search_fields = ("name",)
    list_filter = ("created_at",)
    autocomplete_fields = (
        "register",
        "in_charge",
    )
    inlines = [
        ProcessingCategoryCustomerInline,
        ProcessingCategoryRecipientInline,
        ProcessingCategorySecurityInline,
    ]


@admin.register(BreachRegister)
class BreachRegisterAdmin(admin.ModelAdmin):
    """Gestion des registres de violations."""

    list_display = (
        "name",
        "organisation",
        "representative",
        "dpo",
        "created_at",
        "updated_at",
    )
    search_fields = ("name",)
    list_filter = ("created_at",)
    autocomplete_fields = (
        "representative",
        "dpo",
        "organisation",
    )


@admin.register(Breach)
class BreachAdmin(admin.ModelAdmin):
    """Violation."""

    list_display = (
        "name",
        "register",
        "started_at",
        "assessed_at",
        "created_at",
        "updated_at",
        "confidentiality_breach",
        "integrity_breach",
        "availability_breach",
    )
    search_fields = ("name",)
    list_filter = (
        "created_at",
        "started_at",
        "assessed_at",
        "confidentiality_breach",
        "integrity_breach",
        "availability_breach",
    )
    autocomplete_fields = ("register",)


@admin.register(RequestRegister)
class RequestRegisterAdmin(admin.ModelAdmin):
    """Gestion des registres des demandes."""

    list_display = (
        "name",
        "organisation",
        "created_at",
        "updated_at",
    )
    search_fields = ("name",)
    list_filter = ("created_at",)
    autocomplete_fields = ("organisation",)
