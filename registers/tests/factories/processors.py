"""Générateurs pour les modèles des registres."""
from random import randint
from typing import List

import factory

from registers.models import (
    Customer,
    ProcessingCategory,
    ProcessingCategoryCustomer,
    ProcessingCategoryRecipient,
    ProcessingCategorySecurity,
    ProcessorRegister,
    Recipient,
    Security,
)
from core.tests.factories import ActorFactory, OrganisationFactory
from core.tests.providers import RegisterSentenceProvider
from registers.tests.factories.base import RecipientFactory, SecurityFactory

factory.Faker.add_provider(RegisterSentenceProvider)


class CustomerFactory(factory.django.DjangoModelFactory):
    """Générateur d'Organisation."""

    class Meta:
        model = Customer

    name = factory.Faker("company", locale="fr_FR")
    code = factory.Faker("siren", locale="fr_FR")
    organisation = factory.SubFactory(OrganisationFactory)
    country = "FR"


class ProcessorRegisterFactory(factory.django.DjangoModelFactory):
    """Générateur de registre du sous-traitant."""

    class Meta:
        model = ProcessorRegister

    name = factory.LazyAttribute(
        lambda o: f"Registre des traitements pour {o.organisation}"
    )
    representative = factory.SubFactory(ActorFactory)
    dpo = factory.SubFactory(ActorFactory)
    organisation = factory.SubFactory(OrganisationFactory)


class ProcessingCategoryFactory(factory.django.DjangoModelFactory):
    """Générateur de catégorie de traitement pour un registre du sous-traitant."""

    class Meta:
        model = ProcessingCategory

    name = factory.Faker("processing")
    register = factory.SubFactory(ProcessorRegisterFactory)
    in_charge = factory.SubFactory(ActorFactory)

    @factory.post_generation
    def recipients(self, create: bool, extracted: List[Recipient], **kwargs):
        """Ajout de Recipients.

        Si aucun Recipient n'est passé en ``extracted`` on en crée un nombre aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(1, 2)
            ProcessingCategoryRecipientFactory.create_batch(
                count,
                processing=self,
                recipient__organisation=self.register.organisation,
            )
        else:
            for recipient in extracted:
                ProcessingCategoryRecipientFactory(processing=self, recipient=recipient)

    @factory.post_generation
    def securities(self, create: bool, extracted: List[Security], **kwargs):
        """Ajout de Security.

        Si aucun Security n'est passé en ``extracted`` on en crée un nombre aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(1, 5)
            ProcessingCategorySecurityFactory.create_batch(
                count,
                processing=self,
                security__organisation=self.register.organisation,
            )
        else:
            for security in extracted:
                ProcessingCategorySecurityFactory(processing=self, security=security)

    @factory.post_generation
    def customers(self, create: bool, extracted: List[Customer], **kwargs):
        """Ajout de Customer.

        Si aucun Customer n'est passé en ``extracted`` on en crée un nombre aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(0, 7)
            ProcessingCategoryCustomerFactory.create_batch(
                count,
                processing=self,
                customer__organisation=self.register.organisation,
            )
        else:
            for customer in extracted:
                ProcessingCategoryCustomerFactory(processing=self, customer=customer)


class ProcessingCategoryRecipientFactory(factory.django.DjangoModelFactory):
    """Générateur de Recipient lié à une catégorie traitement."""

    class Meta:
        model = ProcessingCategoryRecipient

    recipient = factory.SubFactory(RecipientFactory)
    processing = factory.SubFactory(ProcessingCategoryFactory)
    details = factory.Faker("sentence", locale="fr_FR")


class ProcessingCategorySecurityFactory(factory.django.DjangoModelFactory):
    """Générateur de Security lié à une catégorie traitement."""

    class Meta:
        model = ProcessingCategorySecurity

    security = factory.SubFactory(SecurityFactory)
    processing = factory.SubFactory(ProcessingCategoryFactory)
    details = factory.Faker("sentence", locale="fr_FR")


class ProcessingCategoryCustomerFactory(factory.django.DjangoModelFactory):
    """Générateur de Customer lié à une catégorie traitement."""

    class Meta:
        model = ProcessingCategoryCustomer

    customer = factory.SubFactory(CustomerFactory)
    processing = factory.SubFactory(ProcessingCategoryFactory)
    details = factory.Faker("sentence", locale="fr_FR")
