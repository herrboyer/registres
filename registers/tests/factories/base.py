"""Générateurs pour les modèles des registres."""

import factory

from registers import models
from core.tests.factories import OrganisationFactory
from core.tests.providers import RegisterSentenceProvider

factory.Faker.add_provider(RegisterSentenceProvider)


class DataFactory(factory.django.DjangoModelFactory):
    """Générateur de Data."""

    class Meta:
        model = models.Data

    name = factory.Faker("data")
    description = factory.Faker("sentence", locale="fr_FR")
    sensitive = factory.Faker("boolean", chance_of_getting_true=10)
    organisation = factory.SubFactory(OrganisationFactory)


class SecurityFactory(factory.django.DjangoModelFactory):
    """Générateur de Security."""

    class Meta:
        model = models.Security

    name = factory.Faker("security")
    description = factory.Faker("sentence", locale="fr_FR")
    organisation = factory.SubFactory(OrganisationFactory)


class PeopleFactory(factory.django.DjangoModelFactory):
    """Générateur de People."""

    class Meta:
        model = models.People

    name = factory.Faker("people")
    description = factory.Faker("sentence", locale="fr_FR")
    organisation = factory.SubFactory(OrganisationFactory)


class RecipientFactory(factory.django.DjangoModelFactory):
    """Générateur de destinataire des données."""

    class Meta:
        model = models.Recipient

    name = factory.Faker("recipient")
    category = factory.Faker(
        "random_element",
        elements=[c[0] for c in models.Recipient.Categories.choices],
    )
    european = factory.Faker("boolean", chance_of_getting_true=80)
    guarantees = factory.Maybe(
        "european",
        yes_declaration=None,
        no_declaration=factory.Faker("sentence", locale="fr_FR"),
    )
    organisation = factory.SubFactory(OrganisationFactory)
