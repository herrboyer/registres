"""Générateurs pour les modèles des registres."""
from random import randint
from typing import List

import factory

from registers.models import (
    ControllerRegister,
    Data,
    People,
    Processing,
    ProcessingData,
    ProcessingPeople,
    ProcessingRecipient,
    ProcessingSecurity,
    Recipient,
    Security,
)
from core.tests.factories import ActorFactory, OrganisationFactory
from core.tests.providers import RegisterSentenceProvider
from registers.tests.factories.base import (
    DataFactory,
    PeopleFactory,
    RecipientFactory,
    SecurityFactory,
)

factory.Faker.add_provider(RegisterSentenceProvider)


class ControllerRegisterFactory(factory.django.DjangoModelFactory):
    """Générateur de registre des traitements."""

    class Meta:
        model = ControllerRegister

    name = factory.LazyAttribute(
        lambda o: f"Registre des traitements pour {o.organisation}"
    )
    representative = factory.SubFactory(ActorFactory)
    dpo = factory.SubFactory(ActorFactory)
    organisation = factory.SubFactory(OrganisationFactory)


class ProcessingFactory(factory.django.DjangoModelFactory):
    """Générateur de fiche du registre dédiée à un traitement."""

    class Meta:
        model = Processing

    name = factory.Faker("processing")
    register = factory.SubFactory(ControllerRegisterFactory)
    in_charge = factory.SubFactory(ActorFactory)
    goals = factory.Faker("sentence", locale="fr_FR")

    @factory.post_generation
    def peoples(self, create: bool, extracted: List[People], **kwargs):
        """Ajout de Peoples.

        Si aucun PeopleCategory n'est passé en ``extracted`` on en crée un nombre
        aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(1, 4)
            ProcessingPeopleFactory.create_batch(
                count, processing=self, people__organisation=self.register.organisation
            )
        else:
            for people in extracted:
                ProcessingPeopleFactory(processing=self, people=people)

    @factory.post_generation
    def recipients(self, create: bool, extracted: List[Recipient], **kwargs):
        """Ajout de Recipients.

        Si aucun Recipient n'est passé en ``extracted`` on en crée un nombre aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(1, 2)
            ProcessingRecipientFactory.create_batch(
                count,
                processing=self,
                recipient__organisation=self.register.organisation,
            )
        else:
            for recipient in extracted:
                ProcessingRecipientFactory(processing=self, recipient=recipient)

    @factory.post_generation
    def securities(self, create: bool, extracted: List[Security], **kwargs):
        """Ajout de Security.

        Si aucun Security n'est passé en ``extracted`` on en crée un nombre aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(1, 5)
            ProcessingSecurityFactory.create_batch(
                count,
                processing=self,
                security__organisation=self.register.organisation,
            )
        else:
            for security in extracted:
                ProcessingSecurityFactory(processing=self, security=security)

    @factory.post_generation
    def datas(self, create: bool, extracted: List[Data], **kwargs):
        """Ajout de Data.

        Si aucun Data n'est passé en ``extracted`` on en crée un nombre aléatoire.
        """
        if not create:
            return

        if not extracted:
            count = randint(1, 5)
            ProcessingDataFactory.create_batch(
                count, processing=self, data__organisation=self.register.organisation
            )
        else:
            for data in extracted:
                ProcessingDataFactory(processing=self, data=data)


class ProcessingPeopleFactory(factory.django.DjangoModelFactory):
    """Générateur de ProcessingPeople lié à un traitement."""

    class Meta:
        model = ProcessingPeople

    people = factory.SubFactory(PeopleFactory)
    processing = factory.SubFactory(ProcessingFactory)
    details = factory.Faker("sentence", locale="fr_FR")


class ProcessingRecipientFactory(factory.django.DjangoModelFactory):
    """Générateur de Recipient lié à un traitement."""

    class Meta:
        model = ProcessingRecipient

    recipient = factory.SubFactory(RecipientFactory)
    processing = factory.SubFactory(ProcessingFactory)
    details = factory.Faker("sentence", locale="fr_FR")


class ProcessingSecurityFactory(factory.django.DjangoModelFactory):
    """Générateur de Security lié à un traitement."""

    class Meta:
        model = ProcessingSecurity

    security = factory.SubFactory(SecurityFactory)
    processing = factory.SubFactory(ProcessingFactory)
    details = factory.Faker("sentence", locale="fr_FR")


class ProcessingDataFactory(factory.django.DjangoModelFactory):
    """Générateur de Data lié à un traitement."""

    class Meta:
        model = ProcessingData

    data = factory.SubFactory(DataFactory)
    ttl = "1"
    processing = factory.SubFactory(ProcessingFactory)
    details = factory.Faker("sentence", locale="fr_FR")
