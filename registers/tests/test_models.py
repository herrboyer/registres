"""Tests modèles."""
import pytest
from django.core.exceptions import ValidationError

pytestmark = [pytest.mark.django_db]


def test_data_str(data):
    assert str(data) == data.name


def test_security_str(security):
    assert str(security) == security.name


def test_people_str(people):
    assert str(people) == people.name


def test_actor_str(actor):
    assert str(actor) == f"{actor.first_name} {actor.last_name}, {actor.company}"
    actor.company = None
    assert str(actor) == f"{actor.first_name} {actor.last_name}"


def test_actor_full_name(actor):
    assert actor.full_name == f"{actor.first_name} {actor.last_name}"
    actor.last_name = ""
    assert actor.full_name == actor.first_name
    actor.last_name = "aaa"
    actor.first_name = ""
    assert actor.full_name == actor.last_name


def test_recipient_str(recipient):
    assert str(recipient) == f"{recipient.name} ({recipient.get_category_display()})"


def test_recipient_not_european_requires_guarantees(recipient):
    try:
        recipient.clean()
    except ValidationError:
        pytest.fail("ne devrait pas lever d'exception")
    recipient.european = False
    recipient.guarantees = None
    with pytest.raises(ValidationError):
        recipient.clean()


def test_controller_register_str(controller_register):
    assert str(controller_register) == controller_register.name


def test_processing_str(processing):
    assert str(processing) == processing.name


def test_customer_str(customer):
    assert str(customer) == customer.name
