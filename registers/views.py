"""Vues.

title, subtitle et menu: ces propriétés / méthodes sont accessibles dans les gabarits
sous `view.`
* view.title est le titre de la page
* view.subtitle est affiché sous le titre
* view.menu défini quel élément du menu de la sidebar doit être actif

TODO:
* mettre en place des controles pour vérifier les droits du user (registre, processing)
  peut être dans les queryset ?
* utiliser https://django-extra-views.readthedocs.io pour simplifier les inlines ?
* optimiser les requêtes (select_related & prefetch_related)
"""

from typing import Dict
from django.views.generic import DetailView, CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.http import Http404, HttpRequest
from .models import ControllerRegister, Processing
from .forms import (
    ProcessingForm,
    ControllerRegisterForm,
    ProcessingPeopleFormset,
    ProcessingDataFormset,
    ProcessingRecipientFormset,
    ProcessingSecurityFormset,
)
from django.db import transaction
from django.utils.translation import gettext as _

PROCESSING_FORMSETS = {
    "people_formset": ProcessingPeopleFormset,
    "data_formset": ProcessingDataFormset,
    "recipient_formset": ProcessingRecipientFormset,
    "security_formset": ProcessingSecurityFormset,
}


class ControllerRegisterView(LoginRequiredMixin, DetailView):
    """Registre de traitement."""

    template_name = "controller.html"
    model = ControllerRegister
    context_object_name = "register"
    pk_url_kwarg = "controller_register_pk"

    subtitle = "Recense l’ensemble des traitements mis en œuvre par votre organisme."
    menu = "controller"

    def title(self) -> str:
        """Titre."""
        return f"Registre de traitement {self.object.organisation}"

    def get_context_data(self, **kwargs) -> Dict:
        """Ajout d`organisation` au contexte."""
        context = super().get_context_data(**kwargs)
        context["processings"] = self.object.processings
        return context

    def get_queryset(self):
        """Filtre les registres possibles sur les organisations de l'utilisateur."""
        return self.model.objects.filter(
            organisation__in=self.request.user.organisation_set.all()
        ).select_related("representative", "dpo")


class ControllerRegisterUpdateView(LoginRequiredMixin, UpdateView):
    """Modification d'un registre de traitement."""

    template_name = "controller_form.html"
    model = ControllerRegister
    form_class = ControllerRegisterForm
    pk_url_kwarg = "controller_register_pk"
    menu = "controller"

    def title(self) -> str:
        """Titre."""
        return f"Modifier le registre de traitement {self.object.organisation}"


class ProcessingDetailView(LoginRequiredMixin, DetailView):
    """Détail d'un traitement."""

    template_name = "processing.html"
    model = Processing
    context_object_name = "processing"
    pk_url_kwarg = "processing_pk"
    menu = "controller"

    def title(self) -> str:
        """Titre."""
        return self.object

    def get(self, request, *args, **kwargs) -> HttpRequest:
        """Avant de répondre on vérifie que l'utilisateur a bien accès au traitement.

        - le traitement appartient au registre
        - l'utilisateur a accès au registre

        Pour des raisons de confidentialité on retourne des 404.
        """
        return_ = super().get(request, *args, **kwargs)
        register = get_object_or_404(
            ControllerRegister, pk=kwargs["controller_register_pk"]
        )
        if (
            self.object.register.pk != register.pk
            or register.organisation not in self.request.user.organisation_set.all()
        ):
            raise Http404("Registre ou traitement invalide")

        return return_

    def get_context_data(self, **kwargs) -> Dict:
        """Ajout d`organisation` au contexte."""
        context = super().get_context_data(**kwargs)
        context["organisation"] = self.object.register.organisation
        return context


class ProcessingCreateView(LoginRequiredMixin, CreateView):
    """Création d'un traitement."""

    template_name = "processing_form.html"
    model = Processing
    form_class = ProcessingForm
    menu = "controller"
    title = _("Nouveau traitement")

    def form_valid(self, form):
        """Traitement du formulaire avant sauvegarde.

        * injection du `register` (qui vient de l'url)
        * sauvegarde des formulaire intégrés (inline)
        """
        register = get_object_or_404(
            ControllerRegister, pk=self.kwargs["controller_register_pk"]
        )
        if register.organisation not in self.request.user.organisation_set.all():
            raise Http404("Registre ou traitement invalide")
        instance = form.save(commit=False)
        with transaction.atomic():
            instance.register = register
            instance.save()
            for __, Formset in PROCESSING_FORMSETS.items():
                formset = Formset(self.request.POST)
                if formset.is_valid():
                    formset.instance = instance
                    formset.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs) -> Dict:
        """Ajout des formset au contexte."""
        context = super().get_context_data(**kwargs)
        for name, Formset in PROCESSING_FORMSETS.items():
            context[name] = Formset()
        return context


class ProcessingUpdateView(LoginRequiredMixin, UpdateView):
    """Modification d'un traitement."""

    template_name = "processing_form.html"
    model = Processing
    form_class = ProcessingForm
    pk_url_kwarg = "processing_pk"
    menu = "controller"

    def title(self) -> str:
        """Titre."""
        return _("Modification du traitement %(processing)s") % {
            "processing": self.object
        }

    def form_valid(self, form):
        """Traitement du formulaire avant sauvegarde.

        * sauvegarde des formulaire intégrés (inline)
        """
        with transaction.atomic():
            form.save()
            for __, Formset in PROCESSING_FORMSETS.items():
                formset = Formset(self.request.POST, instance=self.object)
                if formset.is_valid():
                    formset.save()
        return super().form_valid(form)

    def get_context_data(self, **kwargs) -> Dict:
        """Ajout des formset au contexte."""
        context = super().get_context_data(**kwargs)
        for name, Formset in PROCESSING_FORMSETS.items():
            context[name] = Formset(instance=self.object)
        return context
