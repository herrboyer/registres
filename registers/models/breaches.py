"""Modèles liés au registre des violations.

https://www.cnil.fr/fr/les-violations-de-donnees-personnelles au 29/03/2020

La documentation doit consigner les faits concernant la violation de données à caractère
personnel, ses effets et les mesures prises pour y remédier. Elle peut être contrôlée par
la CNIL dans l’objectif de vérifier le respect des obligations en matière de violations.

En pratique, il est conseillé aux responsables du traitement de recenser l’ensemble des
éléments relatifs aux violations et de s’appuyer sur le formulaire de notification mis en
ligne par la CNIL. Ce formulaire peut en effet servir de canevas pour la documentation
interne, qui peut ainsi constituer un outil unique de gestion de la conformité au RGPD
en matière de violations.

Le registre des violations devrait notamment contenir les éléments suivants :

* la nature de la violation ;
* les catégories et le nombre approximatif des personnes concernées ;
* les catégories et le nombre approximatif de fichiers concernés ;
* les conséquences probables de la violation ;
* les mesures prises pour remédier à la violation et, le cas échéant, pour limiter les
  conséquences négatives de la violation ;
* le cas échéant, la justification de l’absence de notification auprès de la CNIL ou
  d’information aux personnes concernées.
"""
import uuid

from django.db import models
from django.utils.translation import gettext as _

from core.models import Actor, Organisation
from registers.models.base import AbstractEntry


class BreachRegister(AbstractEntry):
    """Registre des violations."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    representative = models.ForeignKey(
        Actor,
        on_delete=models.PROTECT,
        related_name="breach_registers_as_representative",
        verbose_name=_("représentant"),
    )
    dpo = models.ForeignKey(
        Actor,
        on_delete=models.PROTECT,
        related_name="breach_registers_as_dpo",
        verbose_name=_("DPO"),
        null=True,
        blank=True,
    )
    organisation = models.OneToOneField(
        Organisation,
        on_delete=models.CASCADE,
        related_name="breach_register",
        null=False,
        blank=False,
    )
    description = None

    class Meta:
        verbose_name = _("registre des violations")
        verbose_name_plural = _("registres des violations")


class Breach(AbstractEntry):
    """Violation.

    TODO:
    * origines
    * causes
    * natures des données
    * personnes et nombre
    * mesures de sécu
    * lier à un registre de traitement ?
    """

    register = models.ForeignKey(
        BreachRegister,
        on_delete=models.PROTECT,
        related_name="breaches",
        verbose_name=_("registre"),
    )

    started_at = models.DateTimeField(
        _("début"), help_text=_("Date (approximative ou non) du début de la violation")
    )
    ended_at = models.DateTimeField(
        _("fin"),
        help_text=_(
            "Date (approximative ou non) de la fin de la violation (vide si toujours en cours)"
        ),
        null=True,
        blank=True,
    )
    assessed_at = models.DateTimeField(_("prise de connaissance"))
    date_comments = models.TextField(
        _("commentaires sur les dates"),
        help_text=_("Si vous n'êtes pas sûr(e) de ces dates, indiquez-le ici."),
        null=True,
        blank=True,
    )
    assessment_comments = models.TextField(
        _("commentaires sur la prise de connaissance"),
        help_text=_("Dans quelles circonstances avez-vous découvert la violation ?"),
        null=True,
        blank=True,
    )
    confidentiality_breach = models.BooleanField(
        _("Perte de la confidentialité"), default=False
    )
    integrity_breach = models.BooleanField(_("Perte de l'intégrité"), default=False)
    availability_breach = models.BooleanField(
        _("Perte de la disponibilité"), default=False
    )

    class Meta:
        verbose_name = _("violation")
        verbose_name_plural = _("violations")
