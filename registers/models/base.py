"""Modèles commun aux différents registres."""
import uuid

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _

from core.models import Organisation


class AbstractEntry(models.Model):
    """Modèle abstrait des différentes entrées d'un registre de traiement.

    Si le champ description n'est pas utile dans le modèle qu'il étend, ajouter
    ``description = None``
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(_("nom"))
    description = models.TextField(_("description"), null=True, blank=True)
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)

    class Meta:
        abstract = True

    def __str__(self) -> str:
        return self.name


class Data(AbstractEntry):
    """Les catégories de données (identité, localisation, etc.)."""

    sensitive = models.BooleanField(_("sensible"), default=False)
    organisation = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name="datas",
        verbose_name=_("organisation"),
    )

    class Meta:
        verbose_name = _("catégorie de donnée")
        verbose_name_plural = _("catégories de données")


class Security(AbstractEntry):
    """Les catégories de mesures de sécurité."""

    organisation = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name="securities",
        verbose_name=_("organisation"),
    )

    class Meta:
        verbose_name = _("catégorie de mesure de sécurité")
        verbose_name_plural = _("catégories de mesures de sécurité")


class People(AbstractEntry):
    """Catégories de personnes concernées par les traitements."""

    organisation = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name="peoples",
        verbose_name=_("organisation"),
    )

    class Meta:
        verbose_name = _("catégorie de personne concernée")
        verbose_name_plural = _("catégories de personnes concernées")


class Recipient(AbstractEntry):
    """Le destinataire des données."""

    class Categories(models.TextChoices):
        """Enum catégories de destinataires."""

        INTERNAL = "internal", _("Interne")
        EXTERNAL = "external", _("Externe")
        SUBCONTRACTOR = "subcontractor", _("Sous-traitant")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    description = None
    category = models.CharField(
        _("catégorie"),
        choices=Categories.choices,
        default=Categories.INTERNAL,
        max_length=20,
    )
    european = models.BooleanField(_("européen"), default=True)
    guarantees = models.TextField(
        _("garanties"), help_text=_("Requis si non-européen"), null=True, blank=True
    )
    organisation = models.ForeignKey(
        Organisation,
        on_delete=models.CASCADE,
        related_name="recipients",
        verbose_name=_("organisation"),
    )

    class Meta:
        verbose_name = _("destinataire")
        verbose_name_plural = _("destinataires")

    def __str__(self) -> str:
        return f"{self.name} ({self.get_category_display()})"

    def clean(self):
        """Si hors Europe, on doit préciser les garanties."""
        if not self.european and not self.guarantees:
            raise ValidationError(
                {
                    "guarantees": _(
                        "Les données transitent hors de l'UE, il faut préciser les garanties"
                    )
                }
            )
