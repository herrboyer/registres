"""Modèles liés aux registres d'activités de traitement.

https://www.cnil.fr/fr/RGDP-le-registre-des-activites-de-traitement au 29/03/2020 :

Le registre du responsable de traitement doit recenser l’ensemble des traitements mis en
œuvre par votre organisme.

En pratique, une fiche de registre doit donc être établie pour chacune de ces activités.

Ce registre doit comporter le nom et les coordonnées de votre organisme ainsi que, le cas
échéant, de votre représentant, si votre organisme n’est pas établi dans l’Union
européenne, et de votre délégué à la protection des données si vous en disposez.

En outre, pour chaque activité de traitement, la fiche de registre doit comporter au
moins les éléments suivants :

* le cas échéant, le nom et les coordonnées du responsable conjoint du traitement mis en
  œuvre
* les finalités du traitement, l’objectif en vue duquel vous avez collecté ces données
* les catégories de personnes concernées (client, prospect, employé, etc.)
* les catégories de données personnelles (exemples : identité, situation familiale,
  économique ou financière, données bancaires, données de connexion, donnés de
  localisation, etc.)
* les catégories de destinataires auxquels les données à caractère personnel ont été ou
  seront communiquées, y compris les sous-traitants auxquels vous recourez
* les transferts de données à caractère personnel vers un pays tiers ou à une
  organisation internationale et, dans certains cas très particuliers, les garanties
  prévues pour ces transferts ;
* les délais prévus pour l'effacement des différentes catégories de données, c’est-à-dire
  la durée de conservation, ou à défaut les critères permettant de la déterminer
* dans la mesure du possible, une description générale des mesures de sécurité techniques
  et organisationnelles que vous mettez en œuvre
"""
import uuid

from django.db import models
from django.utils.translation import gettext as _

from core.models import Actor, Organisation
from registers.models.base import AbstractEntry, Data, People, Recipient, Security
from django.urls import reverse


class ControllerRegister(AbstractEntry):
    """Registre des traitements."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    representative = models.ForeignKey(
        Actor,
        on_delete=models.PROTECT,
        related_name="controller_register_as_representative",
        verbose_name=_("représentant"),
    )
    dpo = models.ForeignKey(
        Actor,
        on_delete=models.PROTECT,
        related_name="controller_register_as_dpo",
        null=True,
        blank=True,
        verbose_name=_("DPO"),
    )
    organisation = models.OneToOneField(
        Organisation,
        on_delete=models.CASCADE,
        related_name="controller_register",
        null=False,
        blank=False,
        verbose_name=_("organisation"),
    )
    description = None

    class Meta:
        verbose_name = _("registre des traitements")
        verbose_name_plural = _("registres des traitements")

    def get_absolute_url(self):
        """Url pour accéder au registre."""
        return reverse("controller-register", args=[str(self.pk)])

    def get_edit_url(self):
        """Url pour modifier le registre."""
        return reverse("controller-register-update", args=[str(self.pk)])


class Processing(AbstractEntry):
    """Fiche du registre dédiée à un traitement."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    register = models.ForeignKey(
        ControllerRegister,
        on_delete=models.CASCADE,
        related_name="processings",
        verbose_name=_("registre"),
    )
    in_charge = models.ForeignKey(
        Actor, on_delete=models.PROTECT, verbose_name=_("responsable")
    )
    goals = models.TextField(_("objectifs"), null=True, blank=False)
    peoples = models.ManyToManyField(
        People,
        through="ProcessingPeople",
        blank=True,
        verbose_name=_("personnes concernées"),
    )
    recipients = models.ManyToManyField(
        Recipient,
        through="ProcessingRecipient",
        blank=True,
        verbose_name=_("destinataires"),
    )
    securities = models.ManyToManyField(
        Security,
        through="ProcessingSecurity",
        blank=True,
        verbose_name=_("mesures de sécurité"),
    )
    datas = models.ManyToManyField(
        Data, through="ProcessingData", blank=True, verbose_name=_("données")
    )

    class Meta:
        verbose_name = _("traitement")
        verbose_name_plural = _("traitements")

    def get_absolute_url(self):
        """Url pour accéder au traitement."""
        return reverse("processing-detail", args=[str(self.register.pk), str(self.pk)])

    def get_edit_url(self):
        """Url pour modifier le traitement."""
        return reverse("processing-update", args=[str(self.register.pk), str(self.pk)])


class ProcessingM2M(models.Model):
    """Modèle abstrait pour lier les catégories aux traitements.

    Ce sont à chaque fois des relations many2many avec une table intermédiaire pour
    détailler.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    processing = models.ForeignKey(
        Processing, on_delete=models.PROTECT, verbose_name=_("traitement")
    )
    details = models.TextField(_("precisions"), null=True, blank=True)
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)

    class Meta:
        abstract = True


class ProcessingPeople(ProcessingM2M):
    """Lien entre les personnes concernées et un traitement."""

    people = models.ForeignKey(
        People, on_delete=models.PROTECT, verbose_name=_("personne")
    )

    class Meta:
        verbose_name = _("personne concernée par le traitement")
        verbose_name_plural = _("personnes concernées par le traitement")
        unique_together = ("processing", "people")


class ProcessingRecipient(ProcessingM2M):
    """Lien entre les destinataires et un traitement."""

    recipient = models.ForeignKey(
        Recipient, on_delete=models.PROTECT, verbose_name=_("destinataire")
    )

    class Meta:
        verbose_name = _("destinataire du traitement")
        verbose_name_plural = _("destinataires du traitement")
        unique_together = ("processing", "recipient")


class ProcessingSecurity(ProcessingM2M):
    """Lien entre une mesure de sécurité et son application dans un traitement."""

    security = models.ForeignKey(
        Security, on_delete=models.PROTECT, verbose_name=_("mesure de sécurité")
    )

    class Meta:
        verbose_name = _("mesure de sécurité du traitement")
        verbose_name_plural = _("mesures de sécurité du traitement")
        unique_together = ("processing", "security")


class ProcessingData(ProcessingM2M):
    """Lien entre une donnée et son usage dans un traitement."""

    data = models.ForeignKey(Data, on_delete=models.PROTECT, verbose_name=_("donnée"))
    ttl = models.CharField(
        _("durée de conservation"),
        help_text=_("3 mois, 1 an à compter de ..."),
        max_length=200,
    )

    class Meta:
        verbose_name = _("donnée du traitement")
        verbose_name_plural = _("données du traitement")
        unique_together = ("processing", "data")
