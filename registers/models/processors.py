"""Modèles liés au registre du sous-traitant.

https://www.cnil.fr/fr/RGDP-le-registre-des-activites-de-traitement au 29/03/2020 :

Le registre du sous-traitant doit recenser toutes les catégories d'activités de
traitement effectuées pour le compte de vos clients.

En pratique, une fiche de registre doit donc être établie pour chacune de ces catégories
d’activités (hébergement de données, maintenance informatique, service d’envoi de
messages de prospection commerciale, etc.).

Ce registre doit comporter le nom et les coordonnées de votre organisme ainsi que, le cas
échéant, de votre représentant, si votre organisme n’est pas établi dans l’Union
européenne, et de votre délégué à la protection des données si vous en disposez.

Pour chaque catégorie d’activité effectuée pour le compte de clients, il doit contenir
les éléments minimaux suivants :

* le nom et les coordonnées de chaque client, responsable de traitement, pour le compte
  duquel vous traitez les données et, le cas échéant, le nom et les coordonnées de leur
  représentant
* le nom et les coordonnées des sous-traitants auxquels vous-même faites appel dans le
  cadre de cette activité
* les catégories de traitements effectués pour le compte de chacun de vos clients,
  c’est-à-dire les opérations effectivement réalisées pour leur compte (par exemple :
  pour la catégorie « service d’envoi de messages de prospection », il peut s’agir de la
  collecte des adresses mails, de l’envoi sécurisé des messages, de la gestion des
  désabonnements, etc.)
* les transferts de données à caractère personnel vers un pays tiers ou à une
  organisation internationale. Dans les cas très particuliers mentionnés au 2ème alinéa
  de l’article 49.1 (absence de décision d’adéquation en vertu de l’article 45 du RGPD,
  absence des garanties appropriées prévues à l’article 46 du RGPD et inapplicabilité des
  exceptions prévues au 1er alinéa de l’article 49.1), les garanties prévues pour
  encadrer les transferts doivent être mentionnées.
* dans la mesure du possible, une description générale des mesures de sécurité techniques
  et organisationnelles que vous mettez en œuvre.
"""
import uuid

from django.db import models
from django.utils.translation import gettext as _
from django_countries.fields import CountryField

from core.models import Actor, Organisation
from registers.models.base import AbstractEntry, Recipient, Security


class Customer(models.Model):
    """Un client pour lequel une sous-traitance est réalisée.

    On pourrait à terme lier Customer à la vraie Organisation si elle existe,
    ex. si ABC travaille pour la société DEF, et que DEF utilise également registres, un
    lien entre la vraie Organisation DEF et le Customer DEF améliorerait les choses.

    TODO:
    * coordonnées des responsables
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(_("nom"))
    code = models.CharField(
        _("code"),
        help_text=_("SIREN pour les entreprises FR"),
        max_length=30,
        blank=True,
        null=True,
    )
    country = CountryField(_("pays"))
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)
    organisation = models.ForeignKey(
        Organisation,
        verbose_name=_("organisation"),
        on_delete=models.SET_NULL,
        related_name="customers",
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("client")
        verbose_name_plural = _("clients")

    def __str__(self) -> str:
        return self.name


class ProcessorRegister(AbstractEntry):
    """Registre du sous-traitant."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    representative = models.ForeignKey(
        Actor,
        on_delete=models.PROTECT,
        related_name="processor_registers_as_representative",
        verbose_name=_("représentant"),
    )
    dpo = models.ForeignKey(
        Actor,
        verbose_name=_("DPO"),
        on_delete=models.PROTECT,
        related_name="processor_registers_as_dpo",
        null=True,
        blank=True,
    )
    organisation = models.OneToOneField(
        Organisation,
        verbose_name=_("organisation"),
        on_delete=models.CASCADE,
        related_name="processor_register",
        null=False,
        blank=False,
    )
    description = None

    class Meta:
        verbose_name = _("registre du sous-traitant")
        verbose_name_plural = _("registres du sous-traitant")


class ProcessingCategory(AbstractEntry):
    """Fiche du registre dédiée à une catégorie de traitement.

    Permet de décrire une catégorie de traitement (ex. hébergement) pour 0 ou n clients
    (0 voudrait signifiant "tous").
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    register = models.ForeignKey(
        ProcessorRegister,
        on_delete=models.PROTECT,
        related_name="processings",
        verbose_name=_("registre"),
    )
    in_charge = models.ForeignKey(
        Actor, on_delete=models.PROTECT, verbose_name=_("responsable")
    )
    customers = models.ManyToManyField(
        Customer,
        through="ProcessingCategoryCustomer",
        blank=True,
        verbose_name=_("clients"),
    )
    recipients = models.ManyToManyField(
        Recipient,
        through="ProcessingCategoryRecipient",
        blank=True,
        verbose_name=_("destinataires"),
    )
    securities = models.ManyToManyField(
        Security,
        through="ProcessingCategorySecurity",
        blank=True,
        verbose_name=_("mesures de sécurité"),
    )

    class Meta:
        verbose_name = _("catégorie de traitement")
        verbose_name_plural = _("catégories de traitement")


class ProcessingCategoryM2M(models.Model):
    """Modèle abstrait pour lier les catégories aux catégories de traitements.

    Ce sont à chaque fois des relations many2many avec une table intermédiaire pour
    détailler.
    """

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    processing = models.ForeignKey(
        ProcessingCategory, verbose_name=_("traitement"), on_delete=models.PROTECT
    )
    details = models.TextField(_("précisions"), null=True, blank=True)
    created_at = models.DateTimeField(_("création"), auto_now_add=True)
    updated_at = models.DateTimeField(_("mise à jour"), auto_now=True)

    class Meta:
        abstract = True


class ProcessingCategoryRecipient(ProcessingCategoryM2M):
    """Lien entre les destinataires et la catégorie de traitement."""

    recipient = models.ForeignKey(
        Recipient, verbose_name=_("destinataire"), on_delete=models.PROTECT
    )

    class Meta:
        verbose_name = _("destinataire d'une catégorie de traitement")
        verbose_name_plural = _("destinataires de catégories de traitement")
        unique_together = ("processing", "recipient")


class ProcessingCategorySecurity(ProcessingCategoryM2M):
    """Lien entre une mesure de sécurité et la catégorie de traitement."""

    security = models.ForeignKey(
        Security, verbose_name=_("mesure de sécurité"), on_delete=models.PROTECT
    )

    class Meta:
        verbose_name = _("mesure de sécurité d'une catégorie de traitement")
        verbose_name_plural = _("mesures de sécurité de catégories de traitement")
        unique_together = ("processing", "security")


class ProcessingCategoryCustomer(ProcessingCategoryM2M):
    """Lien entre un client et la catégorie de traitement."""

    customer = models.ForeignKey(
        Customer, verbose_name=_("client"), on_delete=models.PROTECT
    )

    class Meta:
        verbose_name = _("client d'une catégorie de traitement")
        verbose_name_plural = _("clients de catégories de traitement")
        unique_together = ("processing", "customer")
