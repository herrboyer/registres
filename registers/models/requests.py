"""Modèles liés au registre des demandes."""
import uuid

from django.db import models
from django.utils.translation import gettext as _

from core.models import Organisation
from registers.models.base import AbstractEntry


class RequestRegister(AbstractEntry):
    """Registre des demandes."""

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    organisation = models.OneToOneField(
        Organisation,
        on_delete=models.CASCADE,
        related_name="request_register",
        null=False,
        blank=False,
    )
    description = None

    class Meta:
        verbose_name = _("registre des demandes")
        verbose_name_plural = _("registres des demandes")
