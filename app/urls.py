"""Routage."""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from core import views as core_views
from registers import views as registers_views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", core_views.HomeView.as_view(), name="home"),
    path(
        "dashboard",
        core_views.DashboardRedirectView.as_view(),
        name="dashboard-redirect",
    ),
    path(
        "organisation/<uuid:organisation_pk>",
        core_views.DashboardView.as_view(),
        name="dashboard",
    ),
    path(
        "traitements/<uuid:controller_register_pk>",
        registers_views.ControllerRegisterView.as_view(),
        name="controller-register",
    ),
    path(
        "traitements/<uuid:controller_register_pk>/modifier",
        registers_views.ControllerRegisterUpdateView.as_view(),
        name="controller-register-update",
    ),
    path(
        "traitements/<uuid:controller_register_pk>/traitement",
        registers_views.ProcessingCreateView.as_view(),
        name="processing-create",
    ),
    path(
        "traitements/<uuid:controller_register_pk>/t/<uuid:processing_pk>",
        registers_views.ProcessingDetailView.as_view(),
        name="processing-detail",
    ),
    path(
        "traitements/<uuid:controller_register_pk>/t/<uuid:processing_pk>/modifier",
        registers_views.ProcessingUpdateView.as_view(),
        name="processing-update",
    ),
    path("comptes/", include("django.contrib.auth.urls")),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
