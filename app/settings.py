"""Paramétrage django.

Les variables sont à définir dans le fichier .env exclu du versionnage.
Un modèle est disponible dans ``env.tpl``
"""
from email.utils import getaddresses
from pathlib import Path

from environs import Env

# Chemins

SETTINGS_PATH = Path(__file__)
PROJECT_PATH = SETTINGS_PATH.parents[1]

# Chargement des variables d'environnement cf. https://github.com/sloria/environs

env = Env()
env.read_env()

# Version & env

ENVIRONMENT = env.str("ENVIRONMENT", default="dev")
VERSION = "0.4.2"  # géré par la CI (étape bumpversion)

# Applications

DJANGO_APPS = [
    "core.site.CoreAdminConfig",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.forms",
]
LOCAL_APPS = ["core", "registers"]
THIRD_PARTY_APPS = [
    "phonenumber_field",
    "django_countries",
    "django_htmx",
]
INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

# Gabarits

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [PROJECT_PATH / "templates"],
        "APP_DIRS": True,
        "OPTIONS": {
            "builtins": ["django.templatetags.static"],
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    }
]
FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

# Auth et sécurité

SECRET_KEY = env.str("SECRET_KEY")
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default=["localhost"])
DEBUG = env.bool("DJANGO_DEBUG", default=False)
AUTH_USER_MODEL = "core.User"
password_validation = "django.contrib.auth.password_validation"
AUTH_PASSWORD_VALIDATORS = [
    {"NAME": f"{password_validation}.UserAttributeSimilarityValidator"},
    {"NAME": f"{password_validation}.MinimumLengthValidator"},
    {"NAME": f"{password_validation}.CommonPasswordValidator"},
    {"NAME": f"{password_validation}.NumericPasswordValidator"},
]
AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]

ADMINS = getaddresses(
    [env.str("ADMINS", default="Administrateur <admin@registres.app>,")]
)
LOGIN_URL = "login"
LOGIN_REDIRECT_URL = "dashboard-redirect"
LOGOUT_REDIRECT_URL = "home"

CSRF_TRUSTED_ORIGINS = env.list(
    "CSRF_TRUSTED_ORIGINS", default=["http://registres.localhost:8080"]
)

# Intergiciels

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django_htmx.middleware.HtmxMiddleware",
]

# Routage, wsgi & asgi

ROOT_URLCONF = "app.urls"
WSGI_APPLICATION = "app.wsgi.application"

APPEND_SLASH = False

SITE_URL = env.str("SITE_URL", default="http://localhost:8000")

# Base de données

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": env.str("POSTGRES_DB", default="registres"),
        "USER": env.str("POSTGRES_USER", default="registres"),
        "PASSWORD": env.str("POSTGRES_PASSWORD", default="registres"),
        "HOST": env.str("POSTGRES_HOST", default="postgres"),
    }
}

# Langues

LANGUAGES = [("fr", "Français")]
LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = USE_TZ = True

# Fichiers statiques, medias et urls

WWW_PATH = PROJECT_PATH.parents[0] / "www"
STATIC_URL = "/static/"
STATIC_ROOT = WWW_PATH / "static"
STATICFILES_DIRS = [PROJECT_PATH / "static"]
MEDIA_URL = "/media/"
MEDIA_PATH = WWW_PATH / "media"
MEDIA_ROOT = MEDIA_PATH.root
PICTURES_FOLDER = "pictures"
PICTURES_PATH = MEDIA_PATH / PICTURES_FOLDER

TAILWIND_INPUT_CSS = "templates/tailwind.css"
TAILWIND_OUTPUT_CSS = "static/css/style.css"


# parfois les fichiers uploadés sont en 600, on force le 644 (c'est le other qui compte)
FILE_UPLOAD_PERMISSIONS = 0o644

# Paramétrage

SENTINEL_USER_EMAIL = env.str(
    "SENTINEL_USER_EMAIL", default="supprimé@registres-rgpd.fr"
)

DEFAULT_ORGANISATION = {
    "name": env.str("DEFAULT_ORGANISATION_NAME", default="Anybox"),
    "code": env.str("DEFAULT_ORGANISATION_CODE", default="528538598"),
    "country": env.str("DEFAULT_ORGANISATION_COUNTRY", default="FR"),
}

# Utilisateur par défaut
# Sera membre de l'organisation DEFAULT_ORGANISATION en tant qu'admin
# FIXME Sera également superuser de la plateforme
DEFAULT_USER = {
    "first_name": env.str("DEFAULT_USER_FIRST_NAME", default="Daniel-Pierre"),
    "last_name": env.str("DEFAULT_USER_LAST_NAME", default="Olivier"),
    "email": env.str("DEFAULT_USER_EMAIL", default="dpo@anybox.fr"),
    "password": env.str("DEFAULT_USER_PASSWORD"),
}
